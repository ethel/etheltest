from sklearn.feature_extraction.text import TfidfVectorizer
import chromadb

client = chromadb.PersistentClient(path="./AISurvey/chroma_db")

coll = client.get_collection("langchain")
data=coll.get()

documents = data["documents"]

for doc in documents:
   cleaned_text = ''.join([char if ord(char) < 128 else ' ' for char in doc])

# Using TF-IDF to extract keywords
   vectorizer = TfidfVectorizer(stop_words='english', max_features=10)
   X = vectorizer.fit_transform([cleaned_text])
   features = vectorizer.get_feature_names_out()
   filtered_features = [feature for feature in features if not feature.isdigit()]
   print(filtered_features)
