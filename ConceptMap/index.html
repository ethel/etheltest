<!DOCTYPE html>
<!--
 Project Ethel
 Draw Course Material Concept Map

 Copyright (C) 2024  Gerd Kortemeyer, ETH Zurich

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Calculations based on
Gerd Kortemeyer, Virtual-Reality graph visualization based on Fruchterman-Reingold using Unity and SteamVR, 
Information Visualization 21(2), 143 — 152 (2022)
-->

<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ethel Course Map</title>
<style>
   body {
      font-family: sans-serif;
   }

  .canvas-container {
    position: relative; /* Container for positioning */
    width: 100%; /* Full width */
    height: 100vh; /* Full viewport height */
  }
  canvas {
    display: block; /* Remove margin below canvas */
    background: white; /* Canvas background */
    width: 100%;
    height: 100%;
  }
  .slider {
    position: absolute; /* Position sliders over canvas */
    left: 20px; /* Distance from left edge */
    z-index: 10; /* Ensure sliders are above canvas */
  }

  #equidistSliderContainer { top: 20px; }
  #dragSliderContainer { top: 50px; }
  #centerspringSliderContainer { top: 80px; }
</style>
</head>
<body>

<div class="canvas-container">
    <canvas id="termsCanvas"></canvas>
    <div class="slider" id="equidistSliderContainer">
        Spacing: <input type="range" id="equidistSlider" min="10" max="200" value="100">
    </div>
    <div class="slider" id="dragSliderContainer">
        Drag: <input type="range" id="dragSlider" min="0.00001" max="0.0002" step="0.00001" value="0.0001">
    </div>
    <div class="slider" id="centerspringSliderContainer">
        Centering: <input type="range" id="centerspringSlider" min="0.1e-8" max="1.5e-8" step="0.1e-8" value="1e-8">
    </div>
</div>


<script>
const delta_t = 100; // Delta time in milliseconds for the animation
const maxfontsize = 32; // Maximum font size
const fontfalloff = 10; // Exponential falloff of font size
const drawthickness = 20; // Maximum thickness

// Adjust these for animation speed and bouncy distance, see paper

let equidist = 100.0;
let drag = 0.0001;
let centerspring = 1e-8;

// The parameters from the paper


let coulomb = 0.0;
let spring = 0.0;

const canvas = document.getElementById('termsCanvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const ctx = canvas.getContext('2d');


// Center point, where attraction comes from

const centerX = canvas.width / 2;
const centerY = canvas.height / 2;

// Would be "height" in the paper

const radius = Math.min(centerX, centerY);


let textObjects = [];
let matrix = [];
let mutual = [];
let maxStrength = 0;
let maxVertex = 0;

const defaultFile = 'noinfo.csv';

function getCSVFileName() {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const fileName = urlParams.get('csv');
  if (fileName && !fileName.includes('/')) {
    return fileName;
  }
  return defaultFile;
}

function calculateFontSize(count) {
  return Math.round(maxfontsize * (1-Math.exp(-fontfalloff*count/maxVertex)));
}

async function loadAndParseCSV(filePath) {
// HEAD request to see if the file exists
try {
    const response = await fetch(filePath, { method: 'HEAD' }); // Use HEAD to get headers only, for efficiency
    if (!response.ok) {
      filePath = defaultFile;
    }
  } catch (error) {
    filePath = defaultFile;
  }
// GET the file
  try {
    const response = await fetch(filePath);
    const data = await response.text();
    const lines = data.split('\n');
    const terms = lines[0].split(',').slice(1); // Remove the first empty cell

    lines.slice(1).forEach((line, rowIndex) => {
      if (line) {
        const parts = line.split(',');
        matrix[rowIndex] = parts.slice(1).map((value) => parseFloat(value));
      }
    });

// Get the maximum strength

    maxStrength = 0;
    for (let i = 0; i < matrix.length; i++) {
       for (let j = i + 1; j < matrix[i].length; j++) {
           maxStrength=Math.max(maxStrength,matrix[i][j]);
       }
       maxVertex=Math.max(maxVertex,matrix[i][i]);
    }
// Calculations from paper
    
    updateParameters();

// Make the objects and put them at rest into random places

    textObjects = terms.map((term, index) => {
      const count = matrix[index][index]; // Diagonal value for the term
      return {
        text: term,
        x: Math.random() * canvas.width,
        y: Math.random() * canvas.height,
        z: (0.5-Math.random()) * radius,
        fontSize: calculateFontSize(count),
        isDragging: false,
        vx: 0, // Velocity in x
        vy: 0, // Velocity in y
        vz: 0, // Velocity in z
        offsetX: 0,
        offsetY: 0,
        redness: 0
      };
    });

    draw();
  } catch (error) {
    console.error('Error loading or parsing CSV:', error);
  }
}

function updateParameters() {
    // Calculations from paper
    coulomb = centerspring * radius * radius * radius / (matrix.length -1);
    spring = coulomb / (maxStrength*equidist * equidist * equidist);
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  // Draw connecting lines
  for (let i = 0; i < matrix.length; i++) {
    for (let j = i + 1; j < matrix[i].length; j++) {
      const strength = drawthickness * matrix[i][j]/maxStrength;
      if (strength > 0) {
        ctx.beginPath();
        ctx.moveTo(textObjects[i].x, textObjects[i].y);
        ctx.lineTo(textObjects[j].x, textObjects[j].y);
        ctx.lineWidth = strength; // Scale line width by the matrix element
        ctx.strokeStyle = 'rgba(0, 0, 0, 0.025)'; // Semi-transparent black lines
        ctx.stroke();
      }
    }
  }
  // Get the redness from weighted average of neigbors
  for (let i = 0; i < matrix.length; i++) {
     if (!textObjects[i].isDragging) {
        redsum=0;
        rednum=0;
        for (let j = 0; j < matrix[i].length; j++) {
            if (i!=j) {
               if (textObjects[j].redness>0) {
                  redsum+=matrix[i][j]*textObjects[j].redness;
                //  redsum+=textObjects[j].redness;
                  rednum++;
               }
            }
        }
        if (rednum>0) {
           textObjects[i].redness=Math.max(textObjects[i].redness,Math.floor(redsum/rednum));
        } else {
           textObjects[i].redness=0;
        }
     }
  }
  // Draw objects
  textObjects.forEach(obj => {
    ctx.font = `${obj.fontSize}px Arial`;
    ctx.fillStyle = `rgb(${obj.redness},0,0)`; 
    ctx.fillText(obj.text, obj.x, obj.y);
  });
}

// Vector functions

// Vector addition
function add(v1, v2) {
    return v1.map((comp, i) => comp + v2[i]);
}

// Vector subtraction
function subtract(v1, v2) {
    return v1.map((comp, i) => comp - v2[i]);
}

// Scalar multiplication
function scalarMult(v, scalar) {
    return v.map(comp => comp * scalar);
}

// Dot product
function dot(v1, v2) {
    return v1.reduce((sum, comp, i) => sum + comp * v2[i], 0);
}

function calc_mutual() {
    for (let i = 0; i < matrix.length; i++) {
        mutual[i]=[0.0,0.0,0.0];
    }
    for (let i = 0; i < matrix.length; i++) {
        for (let j = i + 1; j < matrix[i].length; j++) {
            // Position vector
            let ri = [textObjects[i].x, textObjects[i].y, textObjects[i].z];
            let rj = [textObjects[j].x, textObjects[j].y, textObjects[j].z];

            // Calculate the connecting vector ri-rj
            let connect = subtract(ri, rj);
            let distance = Math.sqrt(dot(connect,connect));
      
            // Avoid division by zero or exploding terms
            distance = Math.max(distance, 0.0001);

            // Add Coulomb repulsive force

            let force=scalarMult(connect,coulomb/(distance*distance*distance));

            // Add spring attractive force

            force=add(force,scalarMult(connect,-matrix[i][j]*spring))

            // Newton 3
            mutual[i] = add(mutual[i],force);
            mutual[j] = add(mutual[j],scalarMult(force, -1));
        }
    }
}

// Animation function
function animate() {
// Get all the mutual forces ahead of time

   calc_mutual();

   center=[centerX,centerY,0]

   for (let i = 0; i < textObjects.length; i++) {
     if (!textObjects[i].isDragging) {
       // Transfer to vectors
       let vv = [textObjects[i].vx, textObjects[i].vy, textObjects[i].vz];
       let rv = [textObjects[i].x, textObjects[i].y, textObjects[i].z];

       // Calculations of acceleration
   
       // Spring force toward center
       let av = scalarMult(subtract(center, rv), spring);

       // Mutual forces

       av=add(av,mutual[i])

       // Drag force

       av=add(av,scalarMult(vv,-drag))

       // Kinematic equations
       vv = add(vv, scalarMult(av, delta_t));
       rv = add(rv, scalarMult(vv, delta_t));

       // Transfer back
       [textObjects[i].vx, textObjects[i].vy, textObjects[i].vz] = vv;
       [textObjects[i].x, textObjects[i].y, textObjects[i].z] = rv;
     }
   }
  
   draw();
}

// Update variables based on slider input
document.getElementById('equidistSlider').addEventListener('input', function() {
    equidist = parseFloat(this.value);
    updateParameters();
});

document.getElementById('dragSlider').addEventListener('input', function() {
    drag = parseFloat(this.value);
    updateParameters();
});

document.getElementById('centerspringSlider').addEventListener('input', function() {
    centerspring = parseFloat(this.value);
    updateParameters();
});

canvas.addEventListener('mousedown', startDrag);
canvas.addEventListener('mousemove', whileDrag);
canvas.addEventListener('mouseup', endDrag);
canvas.addEventListener('mouseout', endDrag);
canvas.addEventListener('touchstart', startDrag);
canvas.addEventListener('touchmove', whileDrag);
canvas.addEventListener('touchend', endDrag);
canvas.addEventListener('touchcancel', endDrag);

function startDrag(e) {
  let mousePos;
  if (e.type === 'touchstart') {
    mousePos = getTouchPos(canvas, e);
  } else {
    mousePos = getMousePos(canvas, e);
    e.preventDefault();
  }

  for (let i = 0; i < textObjects.length; i++) {
    obj = textObjects[i]
    if (isMouseOverText(mousePos, obj)) {
      obj.isDragging = true;
      obj.redness = 255;
      obj.offsetX = mousePos.x - obj.x;
      obj.offsetY = mousePos.y - obj.y;
    }
  }
}

function whileDrag(e) {
  let mousePos;
  if (e.type === 'touchmove') {
    mousePos = getTouchPos(canvas, e);
  } else {
    mousePos = getMousePos(canvas, e);
    e.preventDefault();
  }

  textObjects.forEach(obj => {
    if (obj.isDragging) {
      obj.x = mousePos.x - obj.offsetX;
      obj.y = mousePos.y - obj.offsetY;
      draw();
    }
  });
}

function endDrag(e) {
  if (e.type !== 'touchend' && e.type !== 'touchcancel') {
    e.preventDefault();
  }
  textObjects.forEach(obj => {
    obj.isDragging = false;
    obj.redness=0;
  });
}

function getMousePos(canvasDom, evt) {
  const rect = canvasDom.getBoundingClientRect();
  const scaleX = canvas.width / rect.width;
  const scaleY = canvas.height / rect.height;
  return {
    x: (evt.clientX - rect.left) * scaleX,
    y: (evt.clientY - rect.top) * scaleY
  };
}

function getTouchPos(canvasDom, touchEvent) {
  const rect = canvasDom.getBoundingClientRect();
  return {
    x: (touchEvent.touches[0].clientX - rect.left),
    y: (touchEvent.touches[0].clientY - rect.top)
  };
}

function isMouseOverText(mousePos, obj) {
  ctx.font = `${obj.fontSize}px Arial`;
  const textWidth = ctx.measureText(obj.text).width;
  const textHeight = obj.fontSize; // Approximate height of text
  return mousePos.x > obj.x && mousePos.x < obj.x + textWidth &&
         mousePos.y < obj.y && mousePos.y > obj.y - textHeight;
}

const csvFileName = getCSVFileName(); // Get the CSV file name from the query string
loadAndParseCSV(csvFileName);
// Start animation loop
setInterval(animate, delta_t);
</script>
</body>
</html>
