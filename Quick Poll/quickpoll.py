# Project Ethel
# Server for Quick Polls
#
# Copyright (C) 2025 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from securecalls import make_ssl, auth
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from urllib.parse import urlparse, parse_qs
import logging
import socket
import os
import json
import uuid
from io import BytesIO
import qrcode
from base64 import b64encode
from openai import AzureOpenAI

poll_data = {}

#
# 1) Single place for shared CSS and JS
#
COMMON_CSS = """
body {
  font-family: Arial, sans-serif;
}
.button {
  background-color: #215CAF;
  border-radius: 10px;
  padding: 10px;
  text-decoration: none;
  color: white;
  border: none;
  box-shadow: none;
  cursor: pointer;
}
"""

COMMON_JS = r"""
function showProcessing(btnId, msgId) {
  // Hide the button
  document.getElementById(btnId).style.display = "none";
  // Show the 'Processing...' message
  document.getElementById(msgId).style.display = "block";
}
"""

def render_page(title, body_html):
    """
    Wraps the provided `body_html` inside a standard HTML layout
    that includes the shared CSS and JS.
    """
    return f"""<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{title}</title>
  <style>{COMMON_CSS}</style>
  <script>{COMMON_JS}</script>
</head>
<body>
{body_html}
</body>
</html>"""

#
# 2) Summarization logic (unchanged except for the docstring).
#
def summarize_responses(responses, config_file="../configs/azure_ai_config.txt"):
    """
    Summarize a list of student responses using Azure OpenAI ChatCompletion.
    Returns a string with a short HTML snippet containing the summary.
    """
    server = None
    api_key = None
    with open(config_file, 'r') as file:
        for line in file:
            line = line.strip()
            if line.startswith("server"):
                server = line.split("=", 1)[1].strip()
            elif line.startswith("key"):
                api_key = line.split("=", 1)[1].strip()

    if not server or not api_key:
        return "Error: Missing Azure config values (server or key)."

    client = AzureOpenAI(
        api_version="2023-07-01-preview",
        azure_endpoint=server,
        api_key=api_key
    )

    joined_responses = "\n".join(responses)
    messages = [
        {
            "role": "system",
            "content": "You faithfully summarize student responses to open-ended surveys."
        },
        {
            "role": "user",
            "content": (
                "In the language of the majority of the responses (English, German, or French), summarize the responses,"
                "highlighting majority opinions and striking remarks, but not commenting on them. "
                "If there are no responses, say so. "
                "Keep it inoffensive, as the summary will be publicly displayed.\n\n"
                f"The raw student responses are:\n{joined_responses}"
            )
        }
    ]

    response_gpt = client.chat.completions.create(
        model="Ethel_4o",
        temperature=0.7,
        messages=messages
    )
    summary_text = response_gpt.choices[0].message.content.strip()

    return (
        f"There were {len(responses)} responses.\n"
        f"<p>{summary_text}</p>"
    )

#
# 3) Main HTTP handler
#
class PollServer(BaseHTTPRequestHandler):

    def set_headers(self, status=200, content_type='text/html'):
        self.send_response(status)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Content-Type', content_type)
        self.end_headers()

    def write_html(self, title, body_html, status=200):
        """
        Shortcut to set headers and send a full HTML page with shared CSS/JS.
        """
        self.set_headers(status=status, content_type='text/html')
        page = render_page(title, body_html)
        self.wfile.write(page.encode('utf-8'))

    #
    # 3A) GET requests
    #
    def do_GET(self):
        isauth, user_info = auth(self.headers)
        parsed_url = urlparse(self.path)
        query = parse_qs(parsed_url.query)

        # Instructor (authenticated)
        if isauth:
            if parsed_url.path == '/':
                # Show instructor main page
                body = """
                  <h1>Welcome to Ethel QuickPolls!</h1>
                  <form action="/startpoll" method="post">
                    <button class="button" type="submit">Start Poll</button>
                  </form>
                """
                self.write_html("Instructor Polls", body)
                return

            if parsed_url.path == '/poll':
                poll_id = query.get('poll', [''])[0]
                if poll_id not in poll_data:
                    self.write_html("Not Found", "<h1>Poll not found.</h1>", status=404)
                    return

                poll_url = f"https://{self.headers['Host']}/poll?poll={poll_id}"
                qr_img = qrcode.make(poll_url)
                buffer = BytesIO()
                qr_img.save(buffer, format="PNG")
                base64_qr = b64encode(buffer.getvalue()).decode("utf-8")
                qr_img_tag = f'<img src="data:image/png;base64,{base64_qr}" alt="QR Code"/>'

                body = f"""
                  <h1>Open Poll</h1>
                  <p>Please scan this QR code:</p>
                  {qr_img_tag}

                  <form action="/closepoll" method="post"
                        onsubmit="showProcessing('closepollbutton','processing')">
                    <input type="hidden" name="poll" value="{poll_id}" />
                    <button id="closepollbutton" class="button" type="submit">Close Poll</button>
                  </form>

                  <div id="processing" style="display: none; margin-top: 1em;">
                    <strong>Processing...</strong>
                  </div>
                """
                self.write_html("Poll Control", body)
                return

            # Unknown instructor route
            self.write_html("Not Found", "<h1>Not Found</h1>", status=404)
            return

        # Student (anonymous)
        else:
            if parsed_url.path == '/poll':
                poll_id = query.get('poll', [''])[0]
                if poll_id not in poll_data:
                    self.write_html("Poll Error", "<h1>Poll not found or no longer active.</h1>", status=404)
                    return
                if not poll_data[poll_id]['open']:
                    self.write_html("Poll Closed", "<h1>This poll is closed. Thank you!</h1>")
                    return

                # Show the free-response form
                body = f"""
                  <h1>Share your response</h1>
                  <form action="/submit" method="post"
                        onsubmit="showProcessing('submitButton','processingMsg')">
                    <input type="hidden" name="poll" value="{poll_id}" />
                    <textarea name="response" rows="4" cols="30"
                              placeholder="Type your answer here..."></textarea><br><br>
                    <button id="submitButton" class="button" type="submit">Submit</button>
                  </form>

                  <div id="processingMsg" style="display: none; margin-top: 1em;">
                    <strong>Processing...</strong>
                  </div>
                """
                self.write_html("Student Poll", body)
                return

            # Unknown student route
            self.write_html("Invalid Link", "<h1>You must have a valid poll link.</h1>", status=404)

    #
    # 3B) POST requests
    #
    def do_POST(self):
        content_length = int(self.headers.get('Content-Length', 0))
        post_data = self.rfile.read(content_length).decode('utf-8')
        parsed_url = urlparse(self.path)
        query = parse_qs(post_data)

        isauth, user_info = auth(self.headers)

        def send_text_response(msg, status=200):
            self.write_html("Message", msg, status=status)

        # Instructor
        if isauth:
            if parsed_url.path == '/startpoll':
                new_poll_id = str(uuid.uuid4())
                poll_data[new_poll_id] = {
                    "open": True,
                    "responses": []
                }
                # Redirect to /poll?poll=new_poll_id
                self.send_response(302)
                self.send_header('Location', f'/poll?poll={new_poll_id}')
                self.end_headers()
                return

            if parsed_url.path == '/closepoll':
                poll_id = query.get('poll', [''])[0]
                if poll_id not in poll_data:
                    send_text_response("<h1>Poll not found.</h1>", status=404)
                    return

                # Close the poll
                poll_data[poll_id]['open'] = False

                # Summarize
                responses = poll_data[poll_id]['responses']
                summary = summarize_responses(responses)

                # Memory cleanup: remove from poll_data to avoid leftover
                del poll_data[poll_id]

                # Display final summary
                body = f"""
                  <h1>Poll Summary</h1>
                  {summary}
                  <br/>
                  <a class="button" href="/">Start a new poll</a>
                """
                self.write_html("Poll Summary", body)
                return

            # Unknown instructor POST
            send_text_response("<h1>Unknown or invalid POST request (Instructor).</h1>", status=404)
            return

        # Student
        else:
            if parsed_url.path == '/submit':
                poll_id = query.get('poll', [''])[0]
                student_answer = query.get('response', [''])[0]

                if poll_id not in poll_data or not poll_data[poll_id]['open']:
                    send_text_response("<h1>Poll not found or closed.</h1>", status=404)
                    return

                poll_data[poll_id]['responses'].append(student_answer.strip())
                # Thank you
                send_text_response("<h1>Thank you!</h1>")
                return

            # Unknown student POST
            send_text_response("<h1>Unknown or invalid POST request (Student).</h1>", status=404)

#
# 4) Server startup
#
def run(server_class=ThreadingHTTPServer, handler_class=PollServer, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)

    ssl_context = make_ssl()
    httpd.socket = ssl_context.wrap_socket(httpd.socket, server_side=True)

    container_id = socket.gethostname()
    logging.basicConfig(filename=f"../logs/box_{container_id}.log", level=logging.INFO)
    logging.info("=============== Starting poll server")

    logging.info(f'Starting https on port {port}...')
    try:
        httpd.serve_forever()
    except Exception as e:
        logging.error(f'An unexpected error occurred: {e}', exc_info=True)
    finally:
        httpd.server_close()
        logging.info('Server shutdown completed.')


if __name__ == '__main__':
    run()

