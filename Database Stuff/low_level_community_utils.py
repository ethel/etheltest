# Project Ethel
#
# Low-level routines to directly handle community-related entries in the databases
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
from low_level_database_utils import execute_db_command

# ==============================================================================
# Functions having to do with communities
# Communities would be things like particular offices on campus, courses, course sections, ...
#

# Make a new community, which could be a subcommunity of parentcommunityid
#
def add_new_community(name, parentcommunityid=None):
    """
    Adds a new community with the specified details, ensuring all database operations
    are either all committed or all rolled back if an error occurs. This includes creating
    a new root resource for the community, named after the community's UUID.
    Parameters:
    - name (str): The name of the community.
    - parentcommunityid (UUID|None): The optional UUID of the parent community.
    """
    try:
        with get_db_cursor() as cur:
            # Insert the new community
            community_insert = """
            INSERT INTO Communities (CommunityName, ParentCommunityID)
            VALUES (%s, %s) RETURNING CommunityID;
            """
            cur.execute(community_insert, (name, parentcommunityid))
            new_community_id = cur.fetchone()[0]  # Fetch the UUID of the newly created community

            # Create a new root resource for the community using the new community's UUID as the resource name
            resource_insert = """
            INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, 'community_root') RETURNING ResourceID;
            """
            cur.execute(resource_insert, (str(new_community_id),))
            new_resource_id = cur.fetchone()[0]

            # Update the new community's TopLevelResourceID with the newly created resource ID
            update_community = """
            UPDATE Communities SET TopLevelResourceID = %s WHERE CommunityID = %s;
            """
            cur.execute(update_community, (new_resource_id, new_community_id))

            cur.connection.commit()  # Commit all changes if all operations succeed
            logging.info(f"Created new community '{name}' with ID: {new_community_id} and root resource ID: {new_resource_id}")
            return True, new_community_id
    except Exception as e:
        cur.connection.rollback()  # Rollback the transaction in case of any failure
        logging.error(f"Failed to create community '{name}': {e}")
        return False, f"An error occurred: {e}"

# Retrace the communities and subcommunities, for example exercise groups within sections within courses
#
def get_community_relationship_trail(community_id):
    """
    Retrieves a list of parent community IDs starting from the given community
    up to the top-level ancestor, and returns the TopLevelResourceID of the ultimate ancestor.

    Args:
        community_id (UUID): The starting community ID.

    Returns:
        tuple: (success (bool), message (str or list), top_level_resource_id (UUID or None))
    """
    recursive_query = """
    WITH RECURSIVE ParentChain AS (
        SELECT CommunityID, ParentCommunityID, TopLevelResourceID
        FROM Communities
        WHERE CommunityID = %s
    UNION ALL
        SELECT c.CommunityID, c.ParentCommunityID, c.TopLevelResourceID
        FROM Communities c
        JOIN ParentChain pc ON c.CommunityID = pc.ParentCommunityID
    )
    SELECT CommunityID, TopLevelResourceID FROM ParentChain;
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(recursive_query, (community_id,))
            results = cur.fetchall()
            
            if not results:
                return False, "No community found with the given ID", None

            community_ids = [result[0] for result in results]
            top_level_resource_id = results[-1][1]  # Assume the last one is the top, as it has no parent
            
            return True, community_ids, top_level_resource_id
    except Exception as e:
        logging.error(f"An error occurred while fetching parent communities: {e}")
        return False, f"An error occurred: {e}", None

