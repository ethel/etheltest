# Project Ethel
#
# Low-level routines to directly handle user-related entries in the databases
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
from low_level_database_utils import execute_db_command

# ==============================================================================
# Functions having to do with users
#

# This makes a new user, which is a pretty big deal
#
def add_new_user(auth_internal_user_id, auth_type_name, first_name, last_name):
    """
    Adds a new user with the specified details, ensuring all database operations
    are either all committed or all rolled back if an error occurs. This includes creating
    a new resource and linking it to the user's TopLevelResourceID.
    """
    try:
        with get_db_cursor() as cur:
            # Check if AuthUserID already exists for AuthTypeName
            check_query = """
            SELECT a.AuthUserID FROM AuthenticationUserIDs a
            JOIN AuthenticationTypes t ON a.AuthTypeID = t.AuthTypeID
            WHERE a.AuthUserID = %s AND t.AuthTypeName = %s;
            """
            cur.execute(check_query, (auth_internal_user_id, auth_type_name))
            if cur.fetchone():
                logging.warning(f"User {auth_internal_user_id} with authentication type {auth_type_name} already exists.")
                return False, "User with given authentication details already exists."

            # Insert new user
            user_insert = """
            INSERT INTO Users (FirstName, LastName) VALUES (%s, %s) RETURNING UserID;
            """
            cur.execute(user_insert, (first_name, last_name))
            new_user_id = cur.fetchone()[0]  # Fetch the UUID of the newly created user

            # Insert new resource linked to this user
            resource_insert = """
            INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, 'user_root') RETURNING ResourceID;
            """
            cur.execute(resource_insert, (new_user_id,))
            new_resource_id = cur.fetchone()[0]

            # Update the new user's TopLevelResourceID with the newly created resource ID
            update_user = """
            UPDATE Users SET TopLevelResourceID = %s WHERE UserID = %s;
            """
            cur.execute(update_user, (new_resource_id, new_user_id))

            # Retrieve the AuthTypeID
            get_auth_type_id_query = """
            SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;
            """
            cur.execute(get_auth_type_id_query, (auth_type_name,))
            auth_type_id = cur.fetchone()
            if not auth_type_id:
                raise Exception("Authentication type does not exist")

            # Insert new AuthenticationUserID entry
            auth_user_insert = """
            INSERT INTO AuthenticationUserIDs (AuthUserID, AuthTypeID, UserID)
            VALUES (%s, %s, %s);
            """
            cur.execute(auth_user_insert, (auth_internal_user_id, auth_type_id[0], new_user_id))
            cur.connection.commit()  # Commit all changes if all operations succeed
            logging.info(f"Created user {auth_internal_user_id} with authentication method {auth_type_name} with root resource ID {new_resource_id}: {new_user_id}")
            return True, new_user_id
    except Exception as e:
        cur.connection.rollback()  # Rollback the transaction in case of any failure
        logging.error(f"User {auth_internal_user_id} with authentication method {auth_type_name} not created: {e}")
        return False, f"An error occurred: {e}"


# Example usage:
# update_result = update_user(user_uuid, FirstName="Alice", LastName="Smith")
# print(update_result)
#
def update_user(user_id, **kwargs):
    """
    Update user record in the Users table.
    Args:
    user_id (UUID): The UUID of the user to update.
    **kwargs: Arbitrary keyword arguments representing column names and their new values.

    Returns:
    (bool, str): Tuple of success flag and either result or error message.
    """
    valid_columns = {
        "FirstName", "FirstNameTranscription", "MiddleNames", "MiddleNamesTranscription",
        "LastName", "LastNameTranscription", "GenerationQualifier", "GenerationQualifierTranscription",
        "TopLevelResourceID"
    }
    columns_to_update = {key: kwargs[key] for key in kwargs if key in valid_columns}

    if not columns_to_update:
        return False, "No valid or existing columns provided for update."

    set_clauses = ", ".join([f"{key} = %s" for key in columns_to_update])
    values = list(columns_to_update.values())

    sql = f"UPDATE Users SET {set_clauses} WHERE UserID = %s;"
    values.append(user_id)

    # Now we pass it to your existing database command execution function
    success, result_or_error = execute_db_command(sql, values)
    if success:
        logging.info(f"Successfully updated user {user_id}. Changes: {kwargs}")
        return True, "Update successful."
    else:
        logging.warning(f"Failed to update user {user_id}. Error: {result_or_error}")
        return False, result_or_error

# Fuzzy search
# Performs an AND between all **kwargs
# 
def search_user_by_field(max_output, **kwargs):
    """
    Conducts a fuzzy search on user data based on multiple specified fields using PostgreSQL's similarity function.
    It creates a dynamic SQL query that performs an AND operation between all provided keyword arguments that are valid user fields.
    This function also supports searching transcription fields and joins with related tables (authentication and community) as needed.

    Args:
        max_output (int): Maximum number of results to return.
        **kwargs: Arbitrary keyword arguments representing fields to be searched. The valid fields include:
            - FirstName, MiddleNames, LastName, GenerationQualifier (with their respective transcriptions)
            - AuthUserID, AuthTypeName (from authentication-related tables)
            - CommunityUserID, CommunityName (from community-related tables)

    Returns:
        tuple: (success (bool), result_or_error (mixed))
            - On success: Returns (True, list of results) where results could be an empty list if no matches are found.
            - On failure: Returns (False, error message) describing why the search failed.

    Example:
        search_user_by_field(10, FirstName="John", LastName="Doe")
        This would search for users with first name and last name similar to "John" and "Doe" respectively.

    Note:
        The similarity threshold is hard-coded at 0.1 in this implementation. Adjustments to this value should be made based on specific accuracy needs.
    """
    valid_columns = {
        "FirstName", "MiddleNames", "LastName", "GenerationQualifier",
        "AuthUserID", "AuthTypeName", "CommunityUserID", "CommunityName"
    }
    # Filter and retain only valid columns
    search_params = {key: kwargs[key] for key in kwargs if key in valid_columns and kwargs[key] is not None}

    if not search_params:
        return False, "No valid or existing columns provided for search."

    # Prepare the SQL query components
    select_clauses = ["Users.*"]
    from_clauses = ["Users"]
    where_clauses = []
    values = []
    similarity_clauses = []
    # Make sure we only join the authentication and community tables if needed, but also only once
    added_auth_join = False
    added_community_join = False

    # Handle dual matching for transcribed fields and regular fields
    transcribed_fields = {
        "FirstName": "FirstNameTranscription",
        "MiddleNames": "MiddleNamesTranscription",
        "LastName": "LastNameTranscription",
        "GenerationQualifier": "GenerationQualifierTranscription"
    }

    # Building the dynamic query
    for field, value in search_params.items():
        if field in transcribed_fields:
            # Fields for which transcriptions exist
            transcription_field = transcribed_fields[field]
            where_clauses.append(f"(similarity(Users.{field}, %s) > 0.1 OR similarity(Users.{transcription_field}, %s) > 0.1)")
            similarity_clauses.append(f"(COALESCE(similarity(Users.{field}, %s),0) + COALESCE(similarity(Users.{transcription_field}, %s),0))")
            values.extend([value, value]) 
        elif field in ["AuthUserID", "AuthTypeName"]:
            if not added_auth_join:
                from_clauses.extend([
                    "LEFT JOIN AuthenticationUserIDs ON Users.UserID = AuthenticationUserIDs.UserID",
                    "LEFT JOIN AuthenticationTypes ON AuthenticationUserIDs.AuthTypeID = AuthenticationTypes.AuthTypeID"
                ])
                added_auth_join = True
            where_clauses.append("similarity(AuthenticationTypes.AuthTypeName, %s) > 0.1" if field == "AuthTypeName" else "similarity(AuthenticationUserIDs.AuthUserID, %s) > 0.1")
            similarity_clauses.append("similarity(AuthenticationTypes.AuthTypeName, %s)" if field == "AuthTypeName" else "similarity(AuthenticationUserIDs.AuthUserID, %s)")
            values.append(value)
        elif field in ["CommunityUserID", "CommunityName"]:
            if not added_community_join:
                from_clauses.extend([
                    "LEFT JOIN CommunityUserIDs ON Users.UserID = CommunityUserIDs.UserID",
                    "LEFT JOIN Communities ON CommunityUserIDs.CommunityID = Communities.CommunityID"
                ])
                added_community_join = True
            where_clauses.append("similarity(Communities.CommunityName, %s) > 0.1" if field == "CommunityName" else "similarity(CommunityUserIDs.CommunityUserID, %s) > 0.1")
            similarity_clauses.append("similarity(Communities.CommunityName, %s)" if field == "CommunityName" else "similarity(CommunityUserIDs.CommunityUserID, %s)")
            values.append(value)
        else:
            where_clauses.append(f"similarity(Users.{field}, %s) > 0.1")
            similarity_clauses.append(f"similarity(Users.{field}, %s)")
            values.append(value)


    # Join all WHERE, FROM clauses
    where_statement = ' AND '.join(where_clauses)
    from_statement = ' '.join(from_clauses)

    # Prepare the SQL query with sorting and limit
    similarity_statement = ' + '.join(similarity_clauses) if similarity_clauses else '1'  # Default to 1 if no similarity
    sql = f"SELECT {', '.join(select_clauses)}, ({similarity_statement}) AS relevance FROM {from_statement} WHERE {where_statement} ORDER BY relevance DESC, Users.LastName LIMIT %s;"

    # Doubling the values for each %s in the SQL
    doubled_values = values * 2
    doubled_values.append(max_output)

    logging.info(f"SQL: {sql}")
    logging.info(f"Val: {doubled_values}")

    # Execute the query using the shared database command function
    success, result_or_error = execute_db_command(sql, doubled_values)
    if success:
        return True, result_or_error if result_or_error else []
    else:
        return False, result_or_error
