from pymongo import MongoClient
from init_postgresql import postgresql_client, postgresql_disconnect

# MongoDB connection
def test_mongo_connection():
    try:
        # Change "localhost" and "27017" to your MongoDB host and port if different
        client = MongoClient('mongodb://localhost:27017/')
        # Attempt to fetch the server info (a simple command to test connectivity)
        client.admin.command('ismaster')
        print("MongoDB: Connection successful.")
    except Exception as e:
        print(f"MongoDB: Connection failed. Error: {e}")

# PostgreSQL connection
def direct_postgres_test():
    try:
        print("Connecting directly via psycopg2...")
        cur = postgresql_client();
        print("Connected successfully. Executing query...")
        cur.execute("SELECT 1;")
        result = cur.fetchone()
        print("Direct psycopg2 test result:", result[0])
        postgresql_disconnect(cur);
    except Exception as e:
        print("Direct psycopg2 connection failed:", e)



if __name__ == "__main__":
    test_mongo_connection()
    direct_postgres_test()
