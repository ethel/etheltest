# Project Ethel
#
# Make a connection pool
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
from psycopg2 import pool
import atexit
import logging
from contextlib import contextmanager

# Set up logging configuration (if not set up elsewhere in your application)
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

logging.info("Setting up connection pool ...")
try:
    connection_pool = pool.SimpleConnectionPool(1, 10, user='ethel', password='fortytwo', host='localhost', database='ethel')
except Exception as e:
    logging.error(f"Could not open connection pool: {e}")
    raise RuntimeError("Setting up connection pool failed")  # Ensure this line is correctly indented and outside the logging.error call
logging.info("Connection pool set up.")

@contextmanager
def get_db_connection():
    """Provide a database connection from the pool, ensuring it is returned after use."""
    conn = connection_pool.getconn()
    try:
        yield conn
    finally:
        connection_pool.putconn(conn)

@contextmanager
def get_db_cursor(commit=False):
    """Provide a cursor from a managed database connection, handling commit/rollback as specified."""
    with get_db_connection() as conn:
        cursor = conn.cursor()
        try:
            yield cursor
            if commit:
                conn.commit()
        except Exception as e:  # Consider catching more specific exceptions or psycopg2.Error if you only want to catch database-related errors.
            conn.rollback()
            logging.error(f"Database error: {e}")
            raise
        finally:
            cursor.close()

def cleanup_database():
    """Close the connection pool cleanly on program termination."""
    logging.info("Closing database connection pool ...")
    connection_pool.closeall()
    logging.info("Pool closed.")

# Register the cleanup function to be called on normal program termination
atexit.register(cleanup_database)
