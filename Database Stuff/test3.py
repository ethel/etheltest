from low_level_user_utils import add_new_user, update_user, search_user_by_field
from low_level_auth_utils import add_new_authentication_type, rename_authentication_type
from low_level_community_utils import add_new_community
from setup_tables import setup_database
import logging

# Set up logging to see detailed output during testing
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Initialize the database setup
setup_database()  # Make sure to call the function with ()

# Add a new authentication type and handle the response
success_auth, result_auth = add_new_authentication_type("galaxy")
if success_auth:
    logging.info(f"Authentication type added successfully with ID: {result_auth}")
else:
    logging.error(f"Failed to add authentication type: {result_auth}")

# Add a new user and handle the response
success_user, result_user = add_new_user("zaphod", "galaxy", "Zaphod", "Beeblebrox")
if success_user:
    logging.info(f"User added successfully with ID: {result_user}")

    success_update, result_update = update_user(result_user,FirstNameTranscription="Zappy")

    if success_update:
        logging.info(f"User updated successfully: {result_update}")
    else:
        logging.error(f"Failed to update user: {result_update}")
else:
    logging.error(f"Failed to add user: {result_user}")

# Testing searches

#----

print()

print('FirstName="ZAPHod"')

success_search, result_search = search_user_by_field(100,FirstName="ZAPHod")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="ZaPPy"')

success_search, result_search = search_user_by_field(100,FirstName="ZaPPy")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zaphod",LastName="Beeblebrox"')

success_search, result_search = search_user_by_field(100,FirstName="Zaphod",LastName="Beeblebrox")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",CommunityUserID="zappy42"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",CommunityUserID="zappy42")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="Galaxy"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="Galaxy")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",AuthUserID="zaph"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",AuthUserID="zaph")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",AuthUserID="marvin"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",AuthUserID="marvin")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod",CommunityUserID="zappy42"')

success_search, result_search = search_user_by_field(100,FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod",CommunityUserID="zappy42")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")

#----

print()

print('CommunityUserID="zappy42",FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod"')

success_search, result_search = search_user_by_field(100,CommunityUserID="zappy42",FirstName="Zappy",LastName="Beeblebrox",AuthTypeName="galaxy",AuthUserID="zaphod")

if success_search:
    logging.info(f"Found: {result_search}")
else:
    logging.error(f"Failed to search: {result_search}")
