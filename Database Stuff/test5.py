import random
import logging
from low_level_user_utils import add_new_user, update_user, search_user_by_field
from low_level_auth_utils import add_new_authentication_type
from setup_tables import setup_database

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def setup_auth_types():
    auth_types = ["typeA", "typeB", "typeC"]
    for auth_type in auth_types:
        success, result = add_new_authentication_type(auth_type)
        if success:
            logging.info(f"Authentication type '{auth_type}' added successfully with ID: {result}")
        else:
            logging.error(f"Failed to add authentication type '{auth_type}': {result}")
    return auth_types


def run_fuzzy_search_tests():
    existing_usernames = [f"tricia_{random.randint(0, 999)}" for _ in range(10)]
    non_existing_usernames = [f"nonuser_{random.randint(1000, 2000)}" for _ in range(10)]
    
    for username in existing_usernames:
        success, result = search_user_by_field(3, FirstName=username[:5])
        if success:
            logging.info(f"Search for '{username}': Found {len(result)} results")
        else:
            logging.error(f"Search failed for '{username}': {result}")

    for username in non_existing_usernames:
        success, result = search_user_by_field(3, FirstName=username[:5])
        if success:
            logging.info(f"Search for non-existing '{username}': Found {len(result)} results")
        else:
            logging.error(f"Search failed for non-existing '{username}': {result}")

# Main program execution
if __name__ == "__main__":
    setup_database()
    auth_types = setup_auth_types()
    run_fuzzy_search_tests()

