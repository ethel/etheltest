# Project Ethel
#
# Low-level routines to directly handle resource-related entries in the databases
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
from low_level_database_utils import execute_db_command

# ==============================================================================
# Functions having to do with resources
#

# Make a new resource of resource_type
#
def add_new_resource(resource_name, resource_type):
    """
    Adds a new resource
    """
    insert_query = "INSERT INTO Resources (ResourceName, ResourceType) VALUES (%s, %s) RETURNING ResourceID;"
    success, result = execute_db_command(insert_query, (resource_name, resource_type))
    if success:
        logging.info(f"New resource added with ID: {result[0]}")
        return True, result[0]
    else:
        logging.error(f"Error adding resource: {result}")
        return False, result

# Make a new folder resource
#
def add_new_folder(folder_name):
    """
    Adds a new folder, which is a specific type of resource.
    """
    return add_new_resource(folder_name, 'folder')

# List all active resources in a folder
#
def list_folder_contents(resource_id):
    """
    Lists the contents of a folder identified by resource_id.
    Outputs all rows (child resources) associated with the given resourceID.
    """
    query = """
        SELECT rr.ChildResourceID, r.ResourceName, r.ResourceType, rr.DisplayName, rr.ChildOrder, rr.IsAssessment
        FROM ResourceRelationships rr
        JOIN Resources r ON rr.ChildResourceID = r.ResourceID
        WHERE rr.ParentResourceID = %s AND rr.ChildOrder IS NOT NULL
        ORDER BY rr.ChildOrder;
    """
    try:
        success, results = execute_db_command(query, (resource_id,))
        if success:
            if results:
                return True, results
            else:
                return True, "No contents found."
        else:
            return False, "Failed to retrieve folder contents."
    except Exception as e:
        return False, f"An error occurred while listing folder contents: {e}"

# Get the resource tree, starting at a resource_id
#
def list_folder_tree(resource_id):
    """
    Generates a JSON structure representing the resource tree starting from the given relationship ID.

    Args:
        relationship_id (UUID): The ID of the starting relationship.

    Returns:
        tuple: (success (bool), tree (dict or str))
    """
    def fetch_children(resource_id):
        """
        Recursively fetches children of a given resource to build the tree.

        Args:
            resource_id (UUID): The ID of the parent resource to fetch children for.

        Returns:
            list: A list of child nodes in the tree.
        """
        success, results = list_folder_contents(resource_id)
        if not success:
            return False, results  # Propagate the error message up the call stack

        if isinstance(results, str):  # Handle the "No contents found." message
            return [], None  # Return an empty list for this branch

        nodes = []
        for child in results:
            node = {
                'id': child[0],  # RelationshipID as ID
                'name': child[3]  # DisplayName of the child
            }
            if child[2] == 'folder':  # Check if the resource type is 'folder'
                child_success, child_nodes = fetch_children(child[0])  # Recursive call
                if not child_success:
                    return False, child_nodes  # Propagate the error message up if any
                node['children'] = child_nodes
            nodes.append(node)
        return True, nodes

    root_success, root_nodes = fetch_children(resource_id)
    if not root_success:
        return False, "Failed to build tree."

    return True, root_nodes

# List all inactive resource instances in a folder, in case you want to restore them
#
def list_inactive_folder_contents(resource_id):
    """
    Lists the inactive contents of a folder identified by resource_id.
    Inactive contents are those where ChildOrder is NULL, indicating they are not actively ordered within the folder.
    """
    query = """
        SELECT rr.ChildResourceID, r.ResourceName, r.ResourceType, rr.DisplayName
        FROM ResourceRelationships rr
        JOIN Resources r ON rr.ChildResourceID = r.ResourceID
        WHERE rr.ParentResourceID = %s AND rr.ChildOrder IS NULL
        ORDER BY r.ResourceName; 
    """
    try:
        success, results = execute_db_command(query, (resource_id,))
        if success:
            if results:
                return True, results
            else:
                return True, "No inactive contents found."
        else:
            return False, "Failed to retrieve inactive folder contents."
    except Exception as e:
        return False, f"An error occurred while listing inactive folder contents: {e}"

# List all the leaves of the tree, top down
#
def list_tree_leaves(resource_id):
    """
    Generates an array of all leaf nodes (non-folder resources) from the tree starting from the given resource ID.
    Each leaf node contains the RelationshipID and IsAssessment attribute.

    Args:
        resource_id (UUID): The ID of the starting resource.

    Returns:
        tuple: (success (bool), leaves (list or str))
    """
    def fetch_leaves(resource_id):
        """
        Recursively fetches leaf nodes of a given resource to build the list of leaves.

        Args:
            resource_id (UUID): The ID of the parent resource to fetch leaf nodes for.

        Returns:
            list: A list of dictionaries, each containing RelationshipID and IsAssessment.
        """
        success, results = list_folder_contents(resource_id)
        if not success:
            return False, results  # Propagate the error message up the call stack

        if isinstance(results, str):  # Handle the "No contents found." message
            return [], None  # Return an empty list for this branch

        leaves = []
        for child in results:
            if child[2] != 'folder':  # Check if the resource type is NOT 'folder'
                leaf_node = {
                    'RelationshipID': child[0],  # RelationshipID
                    'IsAssessment': child[5]     # IsAssessment boolean from query
                }
                leaves.append(leaf_node)
            else:
                # Recursive call to fetch leaves from child folders
                child_success, child_leaves = fetch_leaves(child[0])
                if not child_success:
                    return False, child_leaves  # Propagate the error message up if any
                leaves.extend(child_leaves)  # Append leaves from the recursive call

        return True, leaves

    root_success, root_leaves = fetch_leaves(resource_id)
    if not root_success:
        return False, "Failed to retrieve leaves."

    return True, root_leaves

# Insert a new instance of a resource; the instance is then identified by the relationship_id for parameters and data
# Folders may only be used once, other resources over and over
#
def insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, position='before'):
    """
    Helper function to insert a resource before or after a specified order position within a parent.
    Returns the new RelationshipID on success.
    """
    try:
        with get_db_cursor(commit=False) as cur:
            # Check parent and child types
            cur.execute("SELECT ResourceType FROM Resources WHERE ResourceID = %s", (parent_resource_id,))
            result = cur.fetchone()
            if result is None:
                raise ValueError(f"No resource found with ID {parent_resource_id}.")
            parent_type = result[0]
            if parent_type not in ('folder', 'community_root', 'user_root'):
                raise ValueError(f"Invalid parent resource type {parent_type}.")


            # Fetch the child resource type, name, and used status
            cur.execute("SELECT ResourceType, ResourceName, ResourceUsed FROM Resources WHERE ResourceID = %s", (child_resource_id,))
            result = cur.fetchone()
            if result is None:
                raise ValueError(f"No resource found with ID {child_resource_id}.")
            child_type, child_name, child_used = result
            if child_type in ('community_root', 'user_root'):
                raise ValueError(f"Invalid child resource type {child_type}.")

            # Check if the child is a folder and if it has already been used
            if child_type == 'folder' and child_used:
                raise ValueError(f"The folder {child_name} has already been used and cannot be reused.")

            # Determine the display name
            final_display_name = display_name if display_name else child_name
            
            # Shift existing child orders
            shift_direction = " + 1"
            target_order = child_order + (1 if position == 'after' else 0)
            cur.execute(f"""
                UPDATE ResourceRelationships
                SET ChildOrder = ChildOrder{shift_direction}
                WHERE ParentResourceID = %s AND ChildOrder >= %s
            """, (parent_resource_id, target_order))
            logging.info(f"Child orders updated: shift resources at or above order {target_order}")

            new_order = child_order if position == 'before' else child_order + 1
            # Insert the new relationship and return the new RelationshipID
            cur.execute("""
                INSERT INTO ResourceRelationships (ParentResourceID, ChildResourceID, ChildOrder, DisplayName)
                VALUES (%s, %s, %s, %s) RETURNING RelationshipID
            """, (parent_resource_id, child_resource_id, new_order, final_display_name))
            relationship_id = cur.fetchone()[0]  # Fetch the returned RelationshipID
            logging.info(f"New resource relationship inserted with ID: {relationship_id}")

            # Update the ResourceUsed flag if insert is successful
            cur.execute("""
                UPDATE Resources
                SET ResourceUsed = TRUE
                WHERE ResourceID = %s
            """, (child_resource_id,))
            logging.info(f"ResourceUsed flag set to TRUE for ResourceID: {child_resource_id}")

            # Commit the transaction
            cur.connection.commit()
            logging.info(f"Transaction committed successfully. Resource inserted with RelationshipID: {relationship_id}.")
            return True, relationship_id
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return False, f"An error occurred: {e}"

def insert_resource_before(parent_resource_id, child_order, child_resource_id, display_name=None):
    """
    Inserts a new child resource before a specified order position within a parent.
    """
    return insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, 'before')

def insert_resource_after(parent_resource_id, child_order, child_resource_id, display_name=None):
    """
    Inserts a new child resource after a specified order position within a parent.
    """
    return insert_resource_at_position(parent_resource_id, child_order, child_resource_id, display_name, 'after')

# This is how a relationship_id can be "removed" from a folder
#
def remove_relationship_from_folder(relationship_id):
    """
    Removes a child resource from its relationship by setting the ChildOrder to NULL.
    This deactivates the child in the context of its relationship without deleting the record.
    """
    update_query = """
        UPDATE ResourceRelationships
        SET ChildOrder = NULL
        WHERE RelationshipID = %s;
    """
    try:
        success, result = execute_db_command(update_query, (relationship_id,))
        if success:
            logging.info(f"Child resource in relationship {relationship_id} has been deactivated.")
            return True, "Child resource successfully deactivated in its relationship."
        else:
            logging.error(f"Failed to deactivate child resource in relationship: {result}")
            return False, result
    except Exception as e:
        logging.error(f"An error occurred while deactivating child resource in relationship: {e}")
        return False, f"An error occurred: {e}"

# We never really delete a relationship_id, this is how it can be restored to the end of a folder
#
def recover_relationship_to_folder_end(relationship_id):
    """
    Recovers a relationship by assigning it a ChildOrder that is one higher than the highest one currently in the folder.
    This is done by automatically retrieving the ParentResourceID from the existing relationship record.
    """
    try:
        # Retrieve the ParentResourceID and the current highest ChildOrder using the given RelationshipID
        get_parent_and_max_order_query = """
            SELECT r.ParentResourceID, COALESCE(MAX(rr.ChildOrder), 0)
            FROM ResourceRelationships r
            LEFT JOIN ResourceRelationships rr ON r.ParentResourceID = rr.ParentResourceID
            WHERE r.RelationshipID = %s
            GROUP BY r.ParentResourceID;
        """
        with get_db_cursor() as cur:
            cur.execute(get_parent_and_max_order_query, (relationship_id,))
            result = cur.fetchone()
            if not result:
                logging.error("No data found for the given RelationshipID.")
                return False, "No data found for the given RelationshipID."

            parent_resource_id, max_child_order = result
            new_child_order = max_child_order + 1

            # Update the ChildOrder for the relationship
            update_order_query = """
                UPDATE ResourceRelationships
                SET ChildOrder = %s
                WHERE RelationshipID = %s;
            """
            cur.execute(update_order_query, (new_child_order, relationship_id))
            cur.connection.commit()
            logging.info(f"Relationship {relationship_id} has been successfully recovered with new ChildOrder {new_child_order}.")
            return True, "Relationship successfully recovered to the folder."

    except Exception as e:
        logging.error(f"An error occurred while recovering the relationship: {e}")
        return False, f"An error occurred: {e}"

# Moving a resource around within and between folders, identified by relationship_id
# This is how the resource instance takes its parameters and data with it
#
def move_relationship_at_position(relationship_id, new_parent_folder, child_order, position='before'):
    """
    Moves a relationship to a new position in the same or a different folder,
    either before or after a specified order position, while ensuring the new parent
    is of the correct type and the child is not a restricted type like 'community_root' or 'user_root'.
    """
    try:
        with get_db_cursor(commit=False) as cur:
            # Check new parent folder type
            cur.execute("SELECT ResourceType FROM Resources WHERE ResourceID = %s", (new_parent_folder,))
            new_parent_type = cur.fetchone()
            if new_parent_type is None:
                raise ValueError(f"No resource found with ID {new_parent_folder}.")
            if new_parent_type[0] not in ('folder', 'community_root', 'user_root'):
                raise ValueError(f"Invalid new parent resource type {new_parent_type[0]}.")

            # Retrieve current details of the relationship and check child type
            cur.execute("""
                SELECT ParentResourceID, ChildOrder, ResourceType
                FROM ResourceRelationships
                JOIN Resources ON ResourceRelationships.ChildResourceID = Resources.ResourceID
                WHERE RelationshipID = %s
            """, (relationship_id,))
            current_details = cur.fetchone()
            if current_details is None:
                raise ValueError(f"No relationship found with ID {relationship_id}.")

            current_parent_id, current_child_order, child_type = current_details
            if child_type in ('community_root', 'user_root'):
                raise ValueError(f"Invalid child resource type {child_type}.")

            # If moving within the same folder and the position is the same, do nothing
            if current_parent_id == new_parent_folder and current_child_order == child_order:
                logging.info("No move necessary as the relationship is already in the correct position.")
                return True, "No move necessary."

            # Update child orders in the target folder to make room
            shift_direction = " + 1"
            target_order = child_order + (1 if position == 'after' else 0)
            cur.execute(f"""
                UPDATE ResourceRelationships
                SET ChildOrder = ChildOrder{shift_direction}
                WHERE ParentResourceID = %s AND ChildOrder >= %s
            """, (new_parent_folder, target_order))
            logging.info(f"Child orders updated: shift resources at or above order {target_order} in new folder.")

            # Update the relationship to move it to the new folder and position
            new_order = child_order if position == 'before' else child_order + 1
            cur.execute("""
                UPDATE ResourceRelationships
                SET ParentResourceID = %s, ChildOrder = %s
                WHERE RelationshipID = %s
            """, (new_parent_folder, new_order, relationship_id))
            logging.info(f"Relationship {relationship_id} moved to new position {new_order} in folder {new_parent_folder}.")

            # Commit the transaction
            cur.connection.commit()
            logging.info("Transaction committed successfully. Relationship moved.")
            return True, "Relationship moved successfully."
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return False, f"An error occurred: {e}"

def move_relationship_before(relationship_id, new_parent_folder, child_order):
    """
    Moves a relationship to a new position before a specified order within a parent folder.
    """
    return move_relationship_at_position(relationship_id, new_parent_folder, child_order, 'before')

def move_relationship_after(relationship_id, new_parent_folder, child_order):
    """
    Moves a relationship to a new position after a specified order within a parent folder.
    """
    return move_relationship_at_position(relationship_id, new_parent_folder, child_order, 'after')

# This works itself up the folder structure from resource to subsubfolder to subfolder to to community_root
# 
def get_resource_relationship_trail(relationship_id):
    """
    Retrieves an array of RelationshipIDs from the given RelationshipID up to the root resource.

    Args:
        relationship_id (UUID): The ID of the starting resource relationship.

    Returns:
        tuple: (success (bool), message (str or list))
    """
    recursive_query = """
    WITH RECURSIVE RelationshipTrail AS (
        SELECT RelationshipID, ParentResourceID
        FROM ResourceRelationships
        WHERE RelationshipID = %s
    UNION ALL
        SELECT rr.RelationshipID, rr.ParentResourceID
        FROM ResourceRelationships rr
        JOIN RelationshipTrail rt ON rr.ChildResourceID = rt.ParentResourceID
    )
    SELECT RelationshipID FROM RelationshipTrail;
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(recursive_query, (relationship_id,))
            results = cur.fetchall()

            if not results:
                return False, "No relationship found with the given ID"

            relationship_ids = [result[0] for result in results]

            return True, relationship_ids
    except Exception as e:
        logging.error(f"An error occurred while fetching the resource relationship trail: {e}")
        return False, f"An error occurred: {e}"

