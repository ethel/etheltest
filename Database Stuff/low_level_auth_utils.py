# Project Ethel
#
# Low-level routines to directly handle authentication-related database entries
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
from low_level_database_utils import execute_db_command


# ==============================================================================
# Functions having to do with authentication types
# Authentication types would be things like "Switch EduID" or "Active Directory"
#

# This make a new authentication type, probably does not happen often
#
def add_new_authentication_type(auth_type_name):
    """
    Adds a new authentication type if it does not already exist.
    """
    check_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(check_query, (auth_type_name,))
    if success and result:
        logging.warning(f"Authentication type '{auth_type_name}' already exists.")
        return False, "Authentication type already exists."

    insert_query = "INSERT INTO AuthenticationTypes (AuthTypeName) VALUES (%s) RETURNING AuthTypeID;"
    success, result = execute_db_command(insert_query, (auth_type_name,))
    if success:
        logging.info(f"New authentication type added with ID: {result[0]}")
        return True, result[0]
    else:
        logging.error(f"Error adding authentication type: {result}")
        return False, result

# Renaming an authentication type, probably does not happen often
#
def rename_authentication_type(old_name, new_name):
    """
    Renames an authentication type if the new name is not already in use.
    """
    check_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(check_query, (new_name,))
    if success and result:
        logging.warning(f"Authentication type '{new_name}' already exists.")
        return False, "Authentication type already exists."

    update_query = "UPDATE AuthenticationTypes SET AuthTypeName = %s WHERE AuthTypeName = %s RETURNING AuthTypeID;"
    success, result = execute_db_command(update_query, (new_name, old_name))
    if success:
        if result:
            logging.info(f"Authentication type '{old_name}' renamed to '{new_name}' for {result[0]}.")
            return True, result[0]
        else:
            logging.warning(f"No authentication type found with the name '{old_name}'.")
            return False, None
    else:
        logging.error(f"Error renaming authentication type: {result}")
        return False, result

# This gets the user_id behind a username in an authentication type
#
def get_userid_from_auth(auth_type_name, auth_user_id):
    """
    Retrieves the UserID based on the given AuthTypeName and AuthUserID.
    """
    # Get AuthTypeID from AuthTypeName
    get_auth_type_id_query = "SELECT AuthTypeID FROM AuthenticationTypes WHERE AuthTypeName = %s;"
    success, result = execute_db_command(get_auth_type_id_query, (auth_type_name,))
    if success and result:
        auth_type_id = result[0]
    else:
        logging.error(f"Error fetching AuthTypeID for AuthTypeName '{auth_type_name}': {result}")
        return False, f"Error fetching AuthTypeID for AuthTypeName '{auth_type_name}': {result}"

    # Get UserID from AuthTypeID and AuthUserID
    get_user_id_query = """
        SELECT UserID FROM AuthenticationUserIDs 
        WHERE AuthTypeID = %s AND AuthUserID = %s;
    """
    success, result = execute_db_command(get_user_id_query, (auth_type_id, auth_user_id))
    if success:
        if result:
            logging.info(f"UserID for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}' is '{result[0]}'.")
            return True, result[0]
        else:
            logging.warning(f"No UserID found for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}'.")
            return False, None
    else:
        logging.error(f"Error fetching UserID for AuthTypeName '{auth_type_name}' and AuthUserID '{auth_user_id}': {result}")
        return False, result

