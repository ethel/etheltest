# Project Ethel
# Server for Retrieval Augmented Generation
# Opens web socket on port 8000
# Requires
# ../logs mounted to write log file
# ../templates/chat_template.txt
# ../database/chroma_db directory
# Mount at start of docker
#
# Copyright (C) 2023, 2024  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Load library to open Azure LLM connections
#
from openaicalls_v2 import make_llm, HistoryBuffer
#
# SSL
#
from securecalls import make_ssl
#
# Langchain stuff
#
from langchain.schema import StrOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain_community.vectorstores import Chroma
from langchain_core.runnables import RunnableParallel, RunnablePassthrough, RunnableLambda
from langchain_openai import AzureChatOpenAI
from langchain_openai import AzureOpenAIEmbeddings

#
# Websocket stuff
#
import asyncio
import websockets
import json
#
# For docker
#
import socket
#
# Make unique ID
#
import uuid
#
# Get items from JSON
#
from operator import itemgetter
#
# logging
#
import logging
#
# To set environment
#
import os
#
# === Compile the background material for the most recent 
#
def compile_background(chatid):
   global most_recent
   try:
      docs=most_recent[chatid]["context"]
   except:
      return "In the same language as our most recent conversation, tell me that there are no suitable references."
   references=''
   for document in docs:
      references+="\n* "+document.page_content+"\n";
   return "In the same language as our most recent conversation, output the following items as an itemized list:\n"+references
#
# Storing feedback
#
def store_feedback(chatid,good):
   global most_recent
   global store_label
   question="In the same language as our most recent conversation, tell me that you "
   if good:
      question+="were happy to help, "
      with open(f"../logs/good/{store_label}_{chatid}.txt", 'w') as file:
         file.write(str(most_recent))
   else:
      question+="sorry you could not be of more help, "
      with open(f"../logs/bad/{store_label}_{chatid}.txt", 'w') as file:
         file.write(str(most_recent))
   question+="and that my reaction has been stored anonymously."
   return question

#
# === Get all chat history
#
def retrieve_history(chatid):
    global chat_histories
    return chat_histories[chatid].concatenate_items()
#
# Remember what we retrieved
#
def remember(data):
   global most_recent
   most_recent[data['chatid']]=data
   return data
do_remember = RunnableLambda(remember)
#
# ========================================================================= Main program
#
# Get the label for the store
#
store_label = None
with open('../database/label.txt', 'r') as file:
    store_label = file.read().strip()
#
# Set up logging
#
container_id = socket.gethostname()
logging.basicConfig(filename=f"../logs/study.log", level=logging.INFO)
logging.info("=============== Starting server")

#
# Graph Stuff
#
from graphs.qa_ex_gen_graph import graph
from graphs.qa_ex_gen_graph import State
#
# = Initialize stuff
#
# Get LLM and Embedding
#
_, emb = make_llm()
#
# Retrieve the vectorstore with the documents
#
os.environ["ANONYMIZED_TELEMETRY"] = "False"
try:
   vectorstore = Chroma(persist_directory="../database/chroma_db",embedding_function=emb)
   retriever = vectorstore.as_retriever(search_kwargs={"k": 10})
except Exception as error:
   logging.error(f"Opening vectorstore: {error}")
   exit()
#
# Chat histories
#
chat_histories = {}
most_recent = {}

#
# === Coroutine tending to the socket - where actual requests are launched
#
async def handler(websocket,path):
    global chat_histories
    global most_recent
# Check if authenticated
    logging.info("New chat request")
# Get what was sent, should be chatid:question
    try:
       while True:
          logging.info("Getting request")
          client = await websocket.recv()
          data = json.loads(client)  # Parse JSON data
          chatid = data.get("chatid", "").strip()
          question = data.get("content", "")
          loginid = data.get("loginid","")
          logging.info(f"{loginid}: {question}")
          store_this=True
# Catch the control characters
          if (str(question)=='&#128214;'):
             store_this=False
             question=compile_background(chatid);
          if (str(question)=='&#x1F44D;'):
             store_this=False
             question=store_feedback(chatid,True)
          if (str(question)=='&#x1F44E;'):
             store_this=False
             question=store_feedback(chatid,False)
# Unless we have a chatid or the chatid is expired, make a new one
          if not chatid or chatid not in chat_histories:
             chatid=str(uuid.uuid4())
             chat_histories[chatid]=HistoryBuffer()
             logging.info(f"Established chat {chatid}")
# Stream the chain
          repid=str(uuid.uuid4())
          await websocket.send(json.dumps({"chatid": chatid, "repid": repid, "type":"start"}))
          reply=''
# Answer only if user authenticated
          if loginid:
            """
             async for chunk in chain.astream({"question":question,"chatid":chatid}):
                 await websocket.send(json.dumps({"chatid": chatid, "repid": repid, "type":"content", "content": chunk}))
                 reply+=chunk
            """
            response_state = State(**graph.invoke({"question": question,"chatid": chatid, "history": retrieve_history(chatid), "retriever": retriever}))
            reply = response_state.results[-1] # TODO: Handle multiple results # TODO: Do we want to set the exercise main uuid to the repid?
            await websocket.send(json.dumps({"chatid": chatid, "repid": repid, "type":"content", "content": reply})) # TODO: Does reply need to be converted to string/json first if it's an exercise?

          else:
              most_recent[chatid] = {"response": ""}
              reply='Please log in with a correct DESCIL ID.'
              await websocket.send(json.dumps({"chatid": chatid, "repid": repid, "type":"content", "content":reply}))
          await websocket.send(json.dumps({"chatid": chatid, "repid": repid, "type":"end"}))
# Remember full reply for history
          if store_this:
             chat_histories[chatid].add_to_buffer(question,reply)
             most_recent[chatid]["response"]=reply # TODO: Handle case of exercise
    except Exception as error:
       logging.info(f"Handling request: {error}")
       try:
          chatid
       except NameError:
          logging.info("No transactions yet")
       else:
          del chat_histories[chatid]
          del most_recent[chatid]
          logging.info(f"Cleared up transactions from {chatid}")
       await websocket.close()

#
# Set up SSL
#
ssl_context = make_ssl()
#
# We are done initalizing everything we need - now listen to websocket
#
start_server = websockets.serve(handler, "0.0.0.0", 8000, ssl=ssl_context)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
