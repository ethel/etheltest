const radioButtonExercises = [
  {
    uuid: "b1a1d4e5-8b5b-403a-8c12-5912f09f2736",
    text: "Which of the following descriptions most accurately represents the largest planet in the Solar System in terms of its mass, size, and composition?",
    answer_type: "radio_button",
    options: [
      { uuid: "6ec1d2d5-4c49-4e54-a15b-7b1d529e1b29", text: "Earth, which is known for its vibrant ecosystems and ability to support life.", is_correct: false },
      { uuid: "c8d928b4-58c7-4f2b-80f2-c4d3cc4f563d", text: "Mars, often called the Red Planet due to its reddish appearance caused by iron oxide on its surface.", is_correct: false },
      { uuid: "a63fd6ab-4b44-40d6-8cd4-8f5094a0913e", text: "Jupiter, a gas giant primarily composed of hydrogen and helium, with a massive size and Great Red Spot.", is_correct: true },
      { uuid: "e34d5f18-bd2c-4c9b-9206-fd915fb0b8c5", text: "Venus, the hottest planet in our Solar System due to its thick atmosphere and greenhouse effect.", is_correct: false }
    ]
  },
  {
    uuid: "42ad234a-4b67-44a6-9f7b-12d8c9db2c65",
    text: "What is the correct interpretation of the term 'ecosystem' in the context of environmental science?",
    answer_type: "radio_button",
    options: [
      { uuid: "5fd12d48-1fdb-47e6-b2cf-4fdc5d9e12c5", text: "A single species living in an isolated area, without any interaction with other organisms.", is_correct: false },
      { uuid: "d132d5f6-56f7-422d-8712-9c4f53d9b23a", text: "The physical environment and all living organisms interacting in a particular area.", is_correct: true },
      { uuid: "46a5d7c4-89ef-498b-9752-c12f53d7b65f", text: "The set of abiotic factors, such as soil and water, within a habitat.", is_correct: false },
      { uuid: "efb26f5d-47c7-478c-a6f2-b2c459f7d3b4", text: "The processes of photosynthesis and cellular respiration occurring in plants.", is_correct: false }
    ]
  },
  {
    uuid: "98d2f643-1b4d-4f72-8c52-3d8c542e9f64",
    text: "Who among the following authors wrote the book 'War and Peace,' considered one of the greatest works of literature that captures the complexities of human experiences during the Napoleonic Wars?",
    answer_type: "radio_button",
    options: [
      { uuid: "a46d93f5-8b42-412d-8f93-bd4d2d8c573f", text: "Charles Dickens, the English writer known for his vivid depiction of Victorian England.", is_correct: false },
      { uuid: "b4f8d5c7-91ed-4128-b4c5-52e3d9f6b72a", text: "Leo Tolstoy, the Russian novelist celebrated for his depth of insight into human nature.", is_correct: true },
      { uuid: "d546c7f8-36b5-4a67-a2f8-d72c5f36b9e4", text: "Mark Twain, the American humorist and author of 'Adventures of Huckleberry Finn.'", is_correct: false },
      { uuid: "47f8d9e3-53f8-43b4-b9f7-82f4d5c9b3f2", text: "Jane Austen, the English novelist renowned for her social commentary and romantic fiction.", is_correct: false }
    ]
  },
  {
    uuid: "57b9d3f4-2e4b-4216-a3f8-b24f5d38c7b5",
    text: "In physics, what does the term 'entropy' refer to in the context of thermodynamics and the behavior of systems over time?",
    answer_type: "radio_button",
    options: [
      { uuid: "2e9d5f6c-81b5-45f8-b3f9-fb52d3e7c9d4", text: "The amount of energy available to perform useful work in a system.", is_correct: false },
      { uuid: "83f4d5c9-b7f2-498f-9f32-fb7d2c59b3e5", text: "The measure of disorder or randomness in a system.", is_correct: true },
      { uuid: "a2f3d9b5-8b6c-4e92-a7f2-c5d3f72c9d5b", text: "The total energy contained within a closed system.", is_correct: false },
      { uuid: "b5f7d9e4-3c59-44b8-b7d3-f92d8b6f5c3b", text: "The heat transferred in a reversible process at constant temperature.", is_correct: false }
    ]
  },
  {
    uuid: "b1a1d4e5-8b5b-403a-8c12-5912f09f2736",
    text: "What is the largest planet in the Solar System?",
    answer_type: "radio_button",
    options: [
      { uuid: "6ec1d2d5-4c49-4e54-a15b-7b1d529e1b29", text: "Earth", is_correct: false },
      { uuid: "c8d928b4-58c7-4f2b-80f2-c4d3cc4f563d", text: "Mars", is_correct: false },
      { uuid: "a63fd6ab-4b44-40d6-8cd4-8f5094a0913e", text: "Jupiter", is_correct: true },
      { uuid: "e34d5f18-bd2c-4c9b-9206-fd915fb0b8c5", text: "Venus", is_correct: false }
    ]
  },
  {
    uuid: "23fa0341-54f7-4e6c-b2c3-f2cbfc7525b9",
    text: "Which element has the chemical symbol O?",
    answer_type: "radio_button",
    options: [
      { uuid: "f45da8b7-d42f-4c61-a631-97857f3bd1b3", text: "Oxygen", is_correct: true },
      { uuid: "36e1cb3c-9b79-46d5-95b5-d82b3c2b12c1", text: "Hydrogen", is_correct: false },
      { uuid: "d24583ec-dfe5-4451-9954-e2dcf2a9db77", text: "Nitrogen", is_correct: false },
      { uuid: "824e4c94-4b87-40f6-bf92-15f61468c5ec", text: "Carbon", is_correct: false }
    ]
  },
  {
    uuid: "4f7e0b98-858d-441e-930d-dfd891517dc5",
    text: "What is the boiling point of water at sea level?",
    answer_type: "radio_button",
    options: [
      { uuid: "9fa0a1e4-f9c1-4bd1-8161-68112d5d2dbe", text: "90°C", is_correct: false },
      { uuid: "5c216d4e-9b98-4b0c-81e7-b81275f33dbe", text: "100°C", is_correct: true },
      { uuid: "682b412e-bd90-43a1-8c26-7fcb9825d3fc", text: "110°C", is_correct: false },
      { uuid: "db476ae7-f64c-426e-9a12-72dcff9dc524", text: "120°C", is_correct: false }
    ]
  },
  {
    uuid: "be2df4b6-16f8-4a1e-841c-7f5b7b104cdc",
    text: "Who wrote the play 'Hamlet'?",
    answer_type: "radio_button",
    options: [
      { uuid: "b7e6c7d6-58f7-4f2b-8f94-14c3d2179dcb", text: "Oscar Wilde", is_correct: false },
      { uuid: "d4e7a85c-826e-423b-857d-29d5c3a6bf5f", text: "William Shakespeare", is_correct: true },
      { uuid: "82f8c9eb-bd21-4c92-80f9-92dc145e8c5f", text: "Jane Austen", is_correct: false },
      { uuid: "ba7f5a82-967e-445f-88cd-7c5b7e2a8f9e", text: "George Orwell", is_correct: false }
    ]
  }
];

const numericalExercises = [
  {
    uuid: "a7d6c5b3-8c5f-42f8-93d6-d7c3f92b4e5f",
    text: "What is the exact value of the gravitational force acting on an object of mass 10 kilograms at the surface of the Earth? Provide your answer in Newtons (N) and assume the acceleration due to gravity is 9.8 m/s².",
    answer_type: "numerical",
    answer: {
      value: 98,
      unit: "N",
      tolerance: 0.1
    }
  },
  {
    uuid: "d9c8b3f7-72c5-4a8f-94e3-d7b2c46f8d32",
    text: "A car travels 120 kilometers in 2 hours. What is the average speed of the car during this journey? Provide your answer in kilometers per hour (km/h).",
    answer_type: "numerical",
    answer: {
      value: 60,
      unit: "km/h",
      tolerance: 0
    }
  },
  {
    uuid: "e4b7d5f6-81f9-4c8b-a3d9-b74f8c52d9e6",
    text: "The circumference of a circle is calculated using the formula 2πr, where r is the radius. If the radius of the circle is 7 cm, what is the circumference of the circle? Provide your answer in centimeters (cm).",
    answer_type: "numerical",
    answer: {
      value: 43.96,
      unit: "cm",
      tolerance: 0.05
    }
  },
  {
    uuid: "b5f8d9c3-4a7c-47e5-b6d3-92f7c5d2a3e4",
    text: "A chemical reaction releases 250 joules of energy over a duration of 5 seconds. What is the average power output during the reaction? Provide your answer in watts (W).",
    answer_type: "numerical",
    answer: {
      value: 50,
      unit: "W",
      tolerance: 0.5
    }
  },
  {
    uuid: "d2b7c4f5-8462-4a8e-825f-c3d92f4b1e7f",
    text: "What is the approximate value of pi (π)?",
    answer_type: "numerical",
    answer: {
      value: 3.14,
      unit: null,
      tolerance: 0.01
    }
  },
  {
    uuid: "c3a8e2d5-9b1f-48f7-93ec-14d7b2198c4b",
    text: "What is the freezing point of water in degrees Celsius?",
    answer_type: "numerical",
    answer: {
      value: 0,
      unit: "°C",
      tolerance: 0.5
    }
  },
  {
    uuid: "f8b3e5d6-8a7f-4f12-9e92-c7f8d43b7e1d",
    text: "What is the gravitational constant (G) in m^3 kg^-1 s^-2?",
    answer_type: "numerical",
    answer: {
      value: 6.67430,
      unit: "m^3 kg^-1 s^-2",
      tolerance: 0.00001
    }
  },
  {
    uuid: "a7c9b6e4-2f3f-417e-b5e2-df1b9c42c1e8",
    text: "How many days are there in a non-leap year?",
    answer_type: "numerical",
    answer: {
      value: 365,
      unit: "days",
      tolerance: 0
    }
  }
];

module.exports = {
  radioButtonExercises,
  numericalExercises
};

