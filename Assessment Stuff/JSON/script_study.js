/*
 Project Ethel
 Test HTML client script
 Supports multiple WebSocket connections and response selection with conversation continuity

 ... (License information)
*/

// Get references to DOM elements
const chatContainer = document.getElementById("chat-container");
const messageInput = document.getElementById("message-input");
const sendButton = document.getElementById("send-button");

// Parse query parameters
const queryString = window.location.search;
const decodedQueryString = decodeURIComponent(queryString.substring(1));
const queryParams = new URLSearchParams(decodedQueryString);

// Get the station number for the study
let loginParam = queryParams.get('login');
let match = loginParam.match(/(\d+)$/);
let loginID = match ? parseInt(match[1], 10) : null;


// Hack: for this study, we always need the same port
//
let ports = [];
ports = [8055]; // Default port

let regcount = 0;

// Store WebSocket connections and chat IDs
const wsConnections = [];
const chatids = {};

// Flags to control user input
let isAwaitingSelection = false;

// Keep track of the conversation history
const conversationHistory = [];

// Connect WebSockets
function connectWebSockets() {
    ports.forEach((port, index) => {
        connectWebSocket(port, index);
    });
}

function connectWebSocket(port, index) {
    const ws = new WebSocket("wss://" + window.location.hostname + ":" + port + window.location.search);

    ws.onerror = function (error) {
        console.error("WebSocket error on port " + port + ":", error);
    };

    ws.onclose = function () {
        setTimeout(() => connectWebSocket(port, index), 1000); // Reconnect after one second
    };

    ws.onmessage = function (event) {
        displayMessage(event.data, false, index);
    };

    wsConnections[index] = ws;
}

connectWebSockets();

// Functions for handling messages and UI updates
function make_icons_visible() {
    const icons = document.getElementsByClassName("feedback");
    for (let i = 0; i < icons.length; i++) {
        icons[i].style.display = 'inline-block';
    }
}

function make_icons_invisible() {
    const icons = document.getElementsByClassName("feedback");
    for (let i = 0; i < icons.length; i++) {
        icons[i].style.display = 'none';
    }
}

function control(type) {
    switch (type) {
        case 'reference':
            submit('&#128214;');
            break;
        case 'thumbs_up':
            submit('&#x1F44D;');
            break;
        case 'thumbs_down':
            submit('&#x1F44E;');
            break;
    }
}

function escapeHTML(text) {
    if (['&#128214;', '&#x1F44D;', '&#x1F44E;'].includes(text)) {
        return text;
    }
    const map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;',
    };
    return text.replace(/[&<>"']/g, (m) => map[m]);
}

function unescapeHTML(text) {
    const map = {
        '&lt;': '<',
        '&gt;': '>',
        '&amp;': '&',
        '&quot;': '"',
        '&#039;': "'",
    };
    return text.replace(/(&lt;|&gt;|&amp;|&quot;|&#039;)/g, (m) => map[m]);
}

// Event listeners for message input
messageInput.addEventListener("keydown", function (event) {
    if (event.key === "Enter" && !event.shiftKey) {
        event.preventDefault();
        const message = messageInput.value.trim();
        if (message !== "") {
            submit(message);
        }
    }
});

sendButton.addEventListener("click", function () {
    const message = messageInput.value.trim();
    if (message !== "") {
        submit(message);
    }
});

// Submit message to all servers
function submit(message) {
    if (isAwaitingSelection) {
        alert("Please select a response before sending a new message.");
        return;
    }
    if (loginID == null) {
        alert("No ID.");
        return;
    }


    make_icons_invisible();
    regcount += 1;
    const escapedMessage = escapeHTML(message).replace(/\n/g, '<br>');

    // Display user message in the chat container
    displayUserMessage(escapedMessage);

    // Create a new conversation block
    createConversationBlock();

    wsConnections.forEach((ws, index) => {
        if (ws.readyState === WebSocket.OPEN) {
            const data = JSON.stringify({
                "chatid": chatids[index] || '',
		"loginid" : loginID || '',
                "type": "content",
                "repid": "user" + regcount,
                "content": escapedMessage
            });
            ws.send(data);
        } else {
            console.error("WebSocket is not open on port " + ports[index] + ". ReadyState:", ws.readyState);
        }
    });

    messageInput.value = "";
    adjustInputHeight();

    // Disable input until a response is selected
    messageInput.disabled = true;
    sendButton.disabled = true;
    isAwaitingSelection = true;
    updateInputStyles();
}

// Display user message
function displayUserMessage(content) {
    const messageDiv = document.createElement("div");
    messageDiv.classList.add("message", "user-message");
    messageDiv.innerHTML = content;
    chatContainer.appendChild(messageDiv);
    chatContainer.scrollTop = chatContainer.scrollHeight;
}

// Create a new conversation block
function createConversationBlock() {
    const conversationBlock = document.createElement("div");
    conversationBlock.classList.add("conversation-block");
    chatContainer.appendChild(conversationBlock);
    conversationHistory.push(conversationBlock);

    const messagesContainer = document.createElement("div");
    messagesContainer.classList.add("messages-container");
    conversationBlock.appendChild(messagesContainer);

    const numPorts = ports.length;
    const widthPercentage = 100 / numPorts;

    ports.forEach((port, index) => {
        const serverMessagesDiv = document.createElement("div");
        serverMessagesDiv.id = "messages-" + index + "-" + regcount;
        serverMessagesDiv.classList.add("messages");
        serverMessagesDiv.style.width = widthPercentage + "%";
        serverMessagesDiv.style.float = "left";

        // Header to indicate which server
        const header = document.createElement("div");
        header.classList.add("server-header");
        header.innerText = "Response " + (index + 1);
        serverMessagesDiv.appendChild(header);

        messagesContainer.appendChild(serverMessagesDiv);
    });
}

// Display messages from each server separately
function displayMessage(data, isUser, index) {
    const parsedData = JSON.parse(data);
    chatids[index] = parsedData.chatid;
    const content = parsedData.content;
    const type = parsedData.type;
    const repid = parsedData.repid;

    // Use the latest messages container
    const latestConversationBlock = conversationHistory[conversationHistory.length - 1];
    const messagesContainer = latestConversationBlock.querySelector(".messages-container");
    const serverMessagesContainer = messagesContainer.querySelector("#messages-" + index + "-" + regcount);

    if (type === "start") {
        const messageDiv = document.createElement("div");
        messageDiv.id = repid;
        messageDiv.classList.add("message", "server-message");
        serverMessagesContainer.appendChild(messageDiv);

        const newSpinner = document.createElement("div");
        newSpinner.classList.add("spinner");
        newSpinner.id = "spinner";
        messageDiv.appendChild(newSpinner);
    }

    if (type === "content") {
        const messageElement = document.getElementById(repid);
        messageElement.innerHTML += content;
        serverMessagesContainer.scrollTop = serverMessagesContainer.scrollHeight;
    }

    if (type === "end") {
        MathJax.typeset();
        MathJax.typesetClear();
        const messageElement = document.getElementById(repid);
        messageElement.innerHTML = unescapeHTML(messageElement.innerHTML);
        messageElement.innerHTML = marked.parse(messageElement.innerHTML);

        messageElement.querySelectorAll('pre code').forEach((block) => {
            hljs.highlightElement(block);
        });

        const spinner = messageElement.querySelector('#spinner');
        if (spinner) {
            messageElement.removeChild(spinner);
        }

        // Make the entire serverMessagesContainer clickable
        serverMessagesContainer.classList.add("selectable");
        serverMessagesContainer.addEventListener('mouseover', handleMouseOver);
        serverMessagesContainer.addEventListener('mouseout', handleMouseOut);
        serverMessagesContainer.addEventListener('click', handleSelection);
    }
}

// Handle mouse over for selection
function handleMouseOver(event) {
    event.currentTarget.classList.add('highlight');
}

function handleMouseOut(event) {
    event.currentTarget.classList.remove('highlight');
}

// Handle selection of a response
function handleSelection(event) {
    // Remove other panels
    const selectedPanel = event.currentTarget;
    const messagesContainer = selectedPanel.parentElement;
    const panels = messagesContainer.querySelectorAll('.messages');

    panels.forEach((panel) => {
        panel.removeEventListener('mouseover', handleMouseOver);
        panel.removeEventListener('mouseout', handleMouseOut);
        panel.removeEventListener('click', handleSelection);
        panel.classList.remove('selectable', 'highlight');

        if (panel !== selectedPanel) {
            panel.remove(); // Remove other panels
        }
    });

    // Adjust styles of the selected panel
    selectedPanel.style.width = "100%";
    selectedPanel.style.float = "none";

    // Re-enable input for the next prompt
    messageInput.disabled = false;
    sendButton.disabled = false;
    isAwaitingSelection = false;
    updateInputStyles();
}

// Adjust input field styles when disabled/enabled
function updateInputStyles() {
    if (messageInput.disabled) {
        messageInput.classList.add('disabled');
        sendButton.classList.add('disabled');
    } else {
        messageInput.classList.remove('disabled');
        sendButton.classList.remove('disabled');
    }
}

// Function to adjust the height of the text input
function adjustInputHeight() {
    messageInput.style.height = 'auto';
    messageInput.style.height = messageInput.scrollHeight + 'px';
}

messageInput.addEventListener('input', adjustInputHeight);
adjustInputHeight();

