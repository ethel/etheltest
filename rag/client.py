# Project Ethel
# Test client
# Talks to web socket on port 8000
#
# Copyright (C) 2023  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import asyncio
import websockets

async def chat_ws():
    chatid=""
    while True:
        prompt = input("You: ")
        if prompt == 'exit':
           break
        async with websockets.connect("ws://localhost:8000") as websocket:
           await websocket.send(chatid+":"+prompt)
           reply=await websocket.recv()
           parts = reply.split(":", 1)
           chatid=parts[0]
           answer=parts[1] if len(parts) > 1 else ""
           print("Ethel: "+answer)

asyncio.run(chat_ws())
