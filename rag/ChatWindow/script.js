/*
 Project Ethel
 Test HTML client script
 Talks to web socket on port 8000

 Copyright (C) 2024, 2025 Gerd Kortemeyer, ETH Zurich

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
// === DOM ELEMENTS & QUERY PARAMETERS ===

const messagesContainer = document.getElementById("messages");
const messageInput = document.getElementById("message-input");
const sendButton = document.getElementById("send-button");

// Parse query parameters from the URL
const queryParams = new URLSearchParams(window.location.search);
let port = parseInt(queryParams.get('port'));
if (isNaN(port)) {
  port = 8000;
}

let chatid = '';
let regcount = 0;

// Set marked options (used only for server messages) so that newlines become <br>
marked.setOptions({ breaks: true });

// If a "title" GET parameter is provided, display it in the title banner
const titleParam = queryParams.get('title');
if (titleParam) {
  const titleBanner = document.getElementById('title-banner');
  if (titleBanner) {
    titleBanner.textContent = titleParam;
  }
}

// === WEBSOCKET CONNECTION ===

function connectWebSocket() {
  ws = new WebSocket("wss://" + window.location.hostname + ":" + port + window.location.search);
  
  ws.onerror = function(error) {
    console.error("WebSocket error:", error);
  };

  ws.onclose = function() {
    setTimeout(connectWebSocket, 1000); // Reconnect after one second
  };

  ws.onmessage = function(event) {
    // All messages coming from the server are processed as non-user messages
    displayMessage(event.data, false);
  };
}

connectWebSocket();

// === UTILITY FUNCTIONS ===

// Show and hide feedback icons
function make_icons_visible() {
  const icons = document.getElementsByClassName("feedback");
  for (let i = 0; i < icons.length; i++) {
    icons[i].style.display = 'inline-block';
  }
}

function make_icons_invisible() {
  const icons = document.getElementsByClassName("feedback");
  for (let i = 0; i < icons.length; i++) {
    icons[i].style.display = 'none';
  }
}

// Handle control button clicks (the parameters here are already provided as HTML entities)
function control(type) {
  switch (type) {
    case 'reference':
      submit('&#128214;');
      break;
    case 'thumbs_up':
      submit('&#x1F44D;');
      break;
    case 'thumbs_down':
      submit('&#x1F44E;');
      break;
  }
}

// For server messages: unescape HTML entities (so that the server’s content can be correctly parsed)
function unescapeHTML(text) {
  const map = {
    '&lt;': '<',
    '&gt;': '>',
    '&amp;': '&',
    '&quot;': '"',
    '&#039;': "'"
  };
  return text.replace(/(&lt;|&gt;|&amp;|&quot;|&#039;)/g, function(m) { return map[m]; });
}

// === EVENT LISTENERS FOR MESSAGE SUBMISSION ===

// Send message when pressing Enter (without Shift) in the textarea
messageInput.addEventListener("keydown", function(event) {
  if (event.key === "Enter" && !event.shiftKey) {
    event.preventDefault();
    let message = messageInput.value;
    if (message.trim() === "") return;
    submit(message);
  }
});

// Send message when clicking the send button
sendButton.addEventListener("click", function() {
  let message = messageInput.value;
  if (message.trim() === "") return;
  submit(message);
});

// === MESSAGE SUBMISSION & DISPLAY FUNCTIONS ===

/*
 For user messages:
   - We create a single message element (with class "user-message") and set its textContent.
   - The raw text is sent to the server (without escaping), so that quotation marks and other characters remain intact.
   - Because we use textContent, any HTML or LaTeX code appears verbatim.
*/
function submit(message) {
  make_icons_invisible();
  if (ws.readyState === WebSocket.OPEN) {
    regcount++;
    const repid = "user" + regcount;

    // Create and display the user message element using textContent
    const messageDiv = document.createElement("div");
    messageDiv.id = repid;
    messageDiv.classList.add("message", "user-message");
    messageDiv.textContent = message;
    messagesContainer.appendChild(messageDiv);
    messagesContainer.scrollTop = messagesContainer.scrollHeight;

    // Send the raw message to the server (do not escape HTML characters)
    const data = JSON.stringify({
      "chatid": chatid,
      "type": "content",
      "repid": repid,
      "content": message
    });
    ws.send(data);

    messageInput.value = "";
    adjustInputHeight();
  } else {
    console.error("WebSocket is not open. ReadyState:", ws.readyState);
  }
}

/*
 displayMessage(data, isUser)
 
 For user messages (isUser === true):
   - We assume the message element was already created in submit().
   - Any additional content (if sent back) is appended as plain text.
 
 For server messages (isUser === false):
   - The server follows a three-part protocol:
       • "start": create a message element with a spinner.
       • "content": append to the message element’s innerHTML.
       • "end": process the complete content (unescape, Markdown, MathJax, syntax highlighting) and remove the spinner.
*/
function displayMessage(data, isUser) {
  const parsedData = JSON.parse(data);
  chatid = parsedData.chatid;
  const content = parsedData.content;
  const type = parsedData.type;
  const repid = parsedData.repid;

  if (isUser) {
    // For user messages, simply update the textContent (should already be created in submit())
    const messageElement = document.getElementById(repid);
    if (messageElement) {
      messageElement.textContent += content;
      messagesContainer.scrollTop = messagesContainer.scrollHeight;
    }
  } else {
    // For server messages, process according to the type field.
    if (type === "start") {
      const messageDiv = document.createElement("div");
      messageDiv.id = repid;
      messageDiv.classList.add("message", "server-message");
      messagesContainer.appendChild(messageDiv);
      const spinner = document.createElement("div");
      spinner.classList.add("spinner");
      spinner.id = "spinner";
      messageDiv.appendChild(spinner);
    } else if (type === "content") {
      let messageElement = document.getElementById(repid);
      if (messageElement) {
        // Append server content as HTML so that any intended formatting can be applied
        messageElement.innerHTML += content;
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
      }
    } else if (type === "end") {
      const messageElement = document.getElementById(repid);
      MathJax.typeset();
      MathJax.typesetClear();
      // Unescape HTML entities, then process with Markdown
      messageElement.innerHTML = unescapeHTML(messageElement.innerHTML);
      messageElement.innerHTML = marked.parse(messageElement.innerHTML);
      messageElement.querySelectorAll('pre code').forEach((block) => {
        hljs.highlightElement(block);
      });
      // Remove spinner if present
      const spinnerElem = document.getElementById("spinner");
      if (spinnerElem) {
        messageElement.removeChild(spinnerElem);
      }
      make_icons_visible();
    }
  }
}

// === TEXTAREA HEIGHT ADJUSTMENT ===

function adjustInputHeight() {
  messageInput.style.height = 'auto';
  messageInput.style.height = messageInput.scrollHeight + 'px';
}

messageInput.addEventListener('input', adjustInputHeight);
adjustInputHeight();

