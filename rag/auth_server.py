# Project Ethel
# Server for Authentication
# Listens to port 8000
#
# Copyright (C) 2023, 2024  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SSL
#
from securecalls import make_ssl, exchange_token, make_internal_key
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from urllib.parse import urlparse, parse_qs
import logging
import socket

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        client_ip, client_port = self.client_address
        logging.info(f"Request received from {client_ip}:{client_port}")
        host_name = self.headers['Host'].split(':')[0]
        parsed_url = urlparse(self.path)
        query_params = parse_qs(parsed_url.query)
        code = query_params.get('code', [None])[0]

        authenticated = False
        at_home = user_info = id_token = access_token = None

        if code:
            try:
                at_home, user_info, id_token, access_token = exchange_token(code)
                authenticated = access_token is not None
            except Exception as e:
                logging.error(f"Error exchanging token: {e}", exc_info=True)
        else:
            logging.info(f"No code {client_ip}:{client_port}")

        if authenticated:
            logging.info(f"Authenticated {client_ip}:{client_port}")
            if at_home:
                logging.info(f"Granting access {client_ip}:{client_port}")
                token =  make_internal_key(user_info)
                # Send the JWT as a cookie to the client
                self.send_response(302)
                self.send_header('Set-Cookie', f'ethel_session={token}; Domain=ethel.ethz.ch; Path=/; HttpOnly; Secure; SameSite=Lax')
                self.send_header('Location', f'https://{host_name}/login')
                self.end_headers()
            else:
                logging.info(f"Wrong affiliation {client_ip}:{client_port}")
                self.send_response(302)
                self.send_header('Location', f'https://{host_name}/affiliation.html')
                self.end_headers()
        else:
            logging.info(f"Not authenticated {client_ip}:{client_port}")
            self.send_response(302)
            self.send_header('Location', f'https://{host_name}/')
            self.end_headers()

def run(server_class=ThreadingHTTPServer, handler_class=MyServer, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    
    ssl_context = make_ssl()  
    
    httpd.socket = ssl_context.wrap_socket(httpd.socket, server_side=True)
    logging.info(f'Starting httpsd on port {port}...')
    
    try:
        httpd.serve_forever()
    except Exception as e:
        logging.error(f'An unexpected error occurred: {e}', exc_info=True)
    finally:
        httpd.server_close()
        logging.info('Server shutdown completed.')

#
# Main program
#
container_id = socket.gethostname()
logging.basicConfig(filename=f"../logs/auth_{container_id}.log", level=logging.INFO)
logging.info("=============== Starting auth server")
run()
