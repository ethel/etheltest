from openaicalls import make_llm
from langchain.schema import StrOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain.llms import AzureOpenAI

from langchain.vectorstores import Chroma
from langchain.embeddings import OpenAIEmbeddings
from langchain_core.runnables import RunnableParallel, RunnablePassthrough, RunnableLambda

from langchain.document_loaders import TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter


model,emb = make_llm()



vectorstore = Chroma(persist_directory="./chroma_db",embedding_function=emb)
retriever = vectorstore.as_retriever()

template = """Answer the question based only on the following context:
{context}

Question: {question}
"""
prompt = ChatPromptTemplate.from_template(template)

output_parser = StrOutputParser()

setup_and_retrieval = RunnableParallel(
    {"context": retriever, "question": RunnablePassthrough()}
)
chain = setup_and_retrieval | prompt | model | output_parser

print(chain.invoke("Wie wurden die numerischen Daten analysiert?"))
print(chain.invoke("How many students have experience use of AI in teaching situations?"))
