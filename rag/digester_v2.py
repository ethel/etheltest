from openaicalls_cmd_v2 import make_llm
from langchain_community.document_loaders import UnstructuredPDFLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import Chroma
from pathlib import Path

import time

base_dir=Path('./BigData_v2')

model,emb = make_llm()

cnt=0

# Set the batch size and delay time
batch_size = 10  # Adjust this based on your rate limit
delay_time = 2  # Adjust the delay time in seconds

# Function to process documents in batches
def process_in_batches(batch_size, delay_time, filename):
    global cnt
    cnt=cnt+1
    print(f"\n===== {filename}")
    loader = UnstructuredPDFLoader(filename)
    docs = loader.load()
    print("\nStart splitting")
    text_splitter = RecursiveCharacterTextSplitter(
        chunk_size=2000, chunk_overlap=400, add_start_index=True
    )
    print("\nCompiling splits")
    all_splits = text_splitter.split_documents(docs)
    print("\nStart embedding")
    for i in range(0, len(all_splits), batch_size):
        batch = all_splits[i:i + batch_size]
        ids = [str(cnt)+"_"+str(j) for j in range(i, i+batch_size)]
        try:
           print("\n"+str(ids)+"\n")
           Chroma.from_documents(batch,emb,ids=ids,persist_directory="./chroma_db")
        except Exception as error:
           print(f"Handling request: {error}")
           exit()
        time.sleep(delay_time)  # Pause between batches

#
# Main Program
#
for pdf_file in base_dir.rglob('*.pdf'):
     process_in_batches(batch_size, delay_time, str(pdf_file))
print("Done.")


