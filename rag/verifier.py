from openaicalls_cmd import make_llm
from langchain.schema import StrOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain.llms import AzureOpenAI

from langchain.vectorstores import Chroma
from langchain.embeddings import OpenAIEmbeddings
from langchain_core.runnables import RunnableParallel, RunnablePassthrough, RunnableLambda

from langchain.document_loaders import TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter


model,emb = make_llm()


vectorstore = Chroma(persist_directory="./chroma_db",embedding_function=emb)
docs = vectorstore.similarity_search("transportgleichung")
print(docs)
