# Project Ethel
# Set up the LLM chat and embedding
# Just run directly from command line, local config
#
# Copyright (C) 2023  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import os
from langchain_openai import AzureChatOpenAI
from langchain_openai import AzureOpenAIEmbeddings
import logging
#
# === Read the configuration, set required environment variables and maxhistory,
# === return the connections for chat and embeddingy
#

def make_llm(filename="config_v2.txt"):
    deployment = None
    embed = None
    try:
       with open(filename, 'r') as file:
           for line in file:
               line = line.strip()
               value = line.split('=')[1].strip()

               if line.startswith("server ="):
                   os.environ["AZURE_OPENAI_ENDPOINT"] = value

               elif line.startswith("key ="):
                   os.environ["OPENAI_API_KEY"] = value

               elif line.startswith("type ="):
                   os.environ["OPENAI_API_TYPE"] = value

               elif line.startswith("api ="):
                   os.environ["OPENAI_API_VERSION"] = value

               elif line.startswith("deployment ="):  
                   deployment = value

               elif line.startswith("embed ="):
                   embed = value

               elif line.startswith("maxhistory ="):
                   os.environ["LLM_CHAT_MAX_HISTORY"] = value
       if deployment is None or embed is None:
          logging.error("Could not get configuration variables")
          exit()
    except:
       logging.error("Could not open configuration file")
       exit()
    return AzureChatOpenAI(deployment_name=deployment),AzureOpenAIEmbeddings(deployment=embed,chunk_size=1)

#
# === Class for a chat history buffer
# === Keeps only the most recent maxhistory items
#

class HistoryBuffer:

    def __init__(self):
        self.buffer = []
        self.max_size = int(os.environ["LLM_CHAT_MAX_HISTORY"])

    def add_to_buffer(self, human, ai):
        if len(self.buffer) >= self.max_size:
            self.buffer.pop(0)
        self.buffer.append("Human: "+human+"\nAI:"+ai+"\n")

    def concatenate_items(self):
        return "\n\n".join(self.buffer)

    def reset(self):
        self.buffer = []
