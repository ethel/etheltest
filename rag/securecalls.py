# Project Ethel
# Set up the SSL
# Requires file ../configs/ssl_config.txt with the credentials
# Mount ../configs at startup of docker
#
# Copyright (C) 2023, 2024  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
import ssl
import requests
import time
import datetime
import jwt
from jwt.exceptions import InvalidTokenError, ExpiredSignatureError
import uuid
#
oidc_token_endpoint = None
oidc_redirect = None
oidc_client_id = None
oidc_exp_time = None
oidc_user_info = None
private_key_data = None
public_key_data = None
#
# === Read the SSL configuration
# === return the ssl_context
#

def make_ssl(filename="../configs/ssl_config.txt"):
    ssl_cert = None
    ssl_key = None
    global oidc_token_endpoint
    global oidc_redirect
    global oidc_client_id
    global oidc_exp_time
    global oidc_user_info
    global oidc_home
    global oidc_affils
    global private_key_data
    global public_key_data
    try:
       with open(filename, 'r') as file:
           for line in file:
               line = line.strip()
               value = line.split('=')[1].strip()

               if line.startswith("cert ="):
                   ssl_cert = value

               elif line.startswith("key ="):
                   ssl_key = value

               elif line.startswith("oidc_redirect ="):
                   oidc_redirect = value

               elif line.startswith("oidc_token_endpoint ="):
                   oidc_token_endpoint = value

               elif line.startswith("oidc_client_id ="):
                   oidc_client_id = value

               elif line.startswith("oidc_priv_key ="):
                   oidc_priv_key = value

               elif line.startswith("oidc_pub_key ="):
                   oidc_pub_key = value

               elif line.startswith("oidc_exp_time ="):
                   oidc_exp_time = value

               elif line.startswith("oidc_user_info ="):
                   oidc_user_info = value

               elif line.startswith("oidc_home ="):
                   oidc_home = value

               elif line.startswith("oidc_affils ="):
                   oidc_affils = value

       if ssl_cert is None or ssl_key is None:
          logging.error("Could not get SSL configuration variables")
          exit()
       variables=[oidc_redirect, oidc_token_endpoint, oidc_client_id, oidc_priv_key, oidc_pub_key, oidc_exp_time, oidc_user_info, oidc_home, oidc_affils]
       if not all(variable is not None for variable in variables):
          logging.error("Could not get OIDC configuration variables")
          exit()
    except Exception as e:
       logging.error(f"Could not open SSL configuration file: {e}")
       exit()

    try:
       ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
       ssl_context.load_cert_chain(ssl_cert, keyfile=ssl_key)
    except Exception as e:
       logging.error(f"Could not establish SSL context: {e}")
       exit()

# Read the private key
    private_key_data =''
    with open(oidc_priv_key,'r') as file:
        private_key_data = file.read()
    if private_key_data == '':
        logging.error("Could not read private OIDC key")
        exit()

# Read the public key
    public_key_data =''
    with open(oidc_pub_key,'r') as file:
        public_key_data = file.read()
    if public_key_data == '':
        logging.error("Could not read public OIDC key")
        exit()

    return ssl_context
#
# Exchange OIDC code and state for token
#
def exchange_token(code):
    global oidc_token_endpoint
    global oidc_redirect
    global oidc_client_id
    global oidc_exp_time
    global oidc_user_info
    global oidc_home
    global oidc_affils
    global private_key_data

    current_time = int(time.time())
    jwt_id = str(uuid.uuid4())

    jwt_payload = {
       "iss": oidc_client_id,
       "sub": oidc_client_id,
       "aud": oidc_token_endpoint,
       "jti": jwt_id,
       "exp": current_time + int(oidc_exp_time),
       "iat": current_time
    }

    jwt_headers = {
       "alg": "RS256",
       "typ": "JWT",
    }

    client_assertion = jwt.encode(jwt_payload, private_key_data, algorithm="RS256", headers=jwt_headers)

    data = {
       'grant_type': 'authorization_code',
       'client_id': oidc_client_id,
       'code': code,
       'redirect_uri': oidc_redirect,
       'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
       'client_assertion': client_assertion,
    }

# Make the request to exchange the authorization code for a token
    response = requests.post(oidc_token_endpoint, data=data)

# Check if the request was successful
    if response.status_code == 200:
       # Extract the token from the response
       token_info = response.json()
       access_token = token_info.get('access_token')
       id_token = token_info.get('id_token')
       # Get the user info
       headers = {"Authorization": f"Bearer {access_token}"}
       response = requests.get(oidc_user_info, headers=headers)
       if response.status_code == 200:
          user_info = response.json()
          at_home = any(affiliation.endswith('@'+str(oidc_home)) for affiliation in user_info.get(str(oidc_affils),[]))
          return at_home,user_info,id_token,access_token
       else:
          logging.info(f"Failed to fetch user info: {response.status_code} - {response.text}")
          return False,None,None,None
    else:
       logging.info(f"Error exchanging code for token: {response.status_code} - {response.text}")
       return False,None,None,None
#
# Generate the token for this session
#
def make_internal_key(payload):
    global private_key_data
    jwt_payload = {
                    **payload,
                    "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=12)
                  }
    return jwt.encode(jwt_payload, private_key_data, algorithm='RS256')
#
# Read the token and decode it
#
def read_internal_key(token):
    global public_key_data
    try:
        return jwt.decode(token, public_key_data, algorithms=["RS256"], options={"require": ["exp"]})
    except ExpiredSignatureError:
        logging.info("Got an expired signature")
        return None
    except InvalidTokenError:
        logging.info("Got an invalid token")
        return None

#
# Authenticate based on HTTP headers, return the user information (payload of JWT)
#
def auth(headers):
    # Get the cookie from the headers
    cookie = headers.get("Cookie")
    if cookie:
        # Extract the token from the cookie
        cookie_parts = cookie.split(";")
        for part in cookie_parts:
            if "ethel_session" in part:
                token = part.split("=")[1].strip()
                # Check if the token is valid
                if token:
                    user_info = read_internal_key(token)
                    if user_info:
                        return True, user_info
    return False, None

