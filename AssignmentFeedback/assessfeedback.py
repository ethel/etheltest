#!/usr/bin/env python3
import base64
import shutil
from securecalls import make_ssl, auth
from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
from urllib.parse import urlparse, parse_qs
import logging
import socket
import os
import json
import uuid
from io import BytesIO
import cgi
from openai import AzureOpenAI  # if you later use it for LLM calls
import transcribe  # our secondary module for file transcription

# Directories for course configuration and assignment metadata.
COURSE_CONFIG_DIR = os.path.join("..", "course_config")
COURSE_ASSIGNMENTS_DIR = os.path.join("..", "course_assignments")
if not os.path.exists(COURSE_ASSIGNMENTS_DIR):
    os.makedirs(COURSE_ASSIGNMENTS_DIR, exist_ok=True)

#
# 1) Shared CSS and JS for HTML pages.
#
COMMON_CSS = """
body {
  font-family: Arial, sans-serif;
}
.button {
  background-color: #215CAF;
  border-radius: 10px;
  padding: 10px;
  text-decoration: none;
  color: white;
  border: none;
  box-shadow: none;
  cursor: pointer;
  margin: 0.2em;
}
"""
COMMON_JS = r"""
function showProcessing(btnId, msgId) {
  document.getElementById(btnId).style.display = "none";
  document.getElementById(msgId).style.display = "block";
}
"""

def render_page(title, body_html):
    """Wrap provided HTML content in a full HTML document."""
    return f"""<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>{title}</title>
  <style>{COMMON_CSS}</style>
  <script>{COMMON_JS}</script>
</head>
<body>
{body_html}
</body>
</html>"""

#
# Helper function for uniform form value retrieval.
#
def get_form_value(form, key):
    """
    Retrieve a form field's value, working with both cgi.FieldStorage and parse_qs dictionaries.
    """
    if hasattr(form, 'getvalue'):
        return form.getvalue(key)
    else:
        values = form.get(key)
        if values:
            return values[0]
        return None

#
# 2) Utility functions for configuration and assignment metadata.
#
def get_instructor_credentials():
    """
    Reads the instructor credentials from the password file.
    The file should contain two lines: the first is the username,
    and the second is the password.
    """
    path = os.path.join(COURSE_CONFIG_DIR, "password.txt")
    try:
        with open(path, 'r', encoding='utf-8') as f:
            lines = f.read().splitlines()
            if len(lines) < 2:
                logging.error("Password file must contain at least two lines: username and password")
                return None, None
            return lines[0].strip(), lines[1].strip()
    except Exception as e:
        logging.error(f"Error reading instructor credentials: {e}")
        return None, None

def get_course_prompt():
    """
    Reads the course prompt from the prompt file.
    """
    path = os.path.join(COURSE_CONFIG_DIR, "prompt.txt")
    try:
        with open(path, 'r', encoding='utf-8') as f:
            return f.read().strip()
    except Exception as e:
        logging.error(f"Error reading course prompt: {e}")
        return ""

def load_assignments():
    """
    Loads assignment metadata from JSON files stored under COURSE_ASSIGNMENTS_DIR.
    """
    assignments = {}
    for fname in os.listdir(COURSE_ASSIGNMENTS_DIR):
        if fname.startswith("assignment_") and fname.endswith(".json"):
            try:
                with open(os.path.join(COURSE_ASSIGNMENTS_DIR, fname), 'r', encoding='utf-8') as f:
                    data = json.load(f)
                    assignments[data["id"]] = data
            except Exception as e:
                logging.error(f"Error loading assignment {fname}: {e}")
    return assignments

def save_assignment(assignment):
    """
    Saves assignment metadata to a JSON file.
    """
    filename = f"assignment_{assignment['id']}.json"
    path = os.path.join(COURSE_ASSIGNMENTS_DIR, filename)
    try:
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(assignment, f, ensure_ascii=False, indent=2)
    except Exception as e:
        logging.error(f"Error saving assignment {assignment['id']}: {e}")

def delete_assignment(assignment_id):
    """
    Deletes the assignment metadata JSON file.
    (Instructor-provided documents are no longer stored.)
    """
    filename = f"assignment_{assignment_id}.json"
    path = os.path.join(COURSE_ASSIGNMENTS_DIR, filename)
    try:
        if os.path.exists(path):
            os.remove(path)
    except Exception as e:
        logging.error(f"Error deleting assignment {assignment_id}: {e}")

#
# 3) Generate feedback using an LLM.
#
def generate_feedback(course_prompt, sheet_transcription, additional_criteria, solution_content):
    """
    Generates feedback for a student's solution using Azure OpenAI.
    
    The prompt is constructed from the course prompt, the transcribed assignment sheet,
    additional grading instructions, and the student's solution transcription.
    
    Returns:
      A LaTeX-formatted string containing the feedback.
    """
    # Read Azure configuration.
    server = None
    api_key = None
    config_file = "../configs/azure_ai_config_o3.txt"
    try:
        with open(config_file, 'r') as file:
            for line in file:
                line = line.strip()
                if line.startswith("server"):
                    server = line.split("=", 1)[1].strip()
                elif line.startswith("key"):
                    api_key = line.split("=", 1)[1].strip()
    except Exception as e:
        logging.error(f"Error reading Azure config: {e}")
        return "Error: Unable to read Azure config."

    if not server or not api_key:
        logging.error("Missing Azure config values (server or key).")
        return "Error: Missing Azure config values (server or key)."

    try:
        client = AzureOpenAI(
            api_version="2024-12-01-preview",
            azure_endpoint=server,
            api_key=api_key
        )
    except Exception as e:
        logging.error(f"Error initializing AzureOpenAI client: {e}")
        return "Error: Unable to initialize AzureOpenAI client."
    
    # Build the prompt message.
    messages = [
        {"role": "system", "content": "You provide feedback on homework solutions."},
        {"role": "user", "content": f"{course_prompt}\nAssignment sheet: {sheet_transcription}\nAdditional criteria: {additional_criteria}\nStudent solution:\n{solution_content}"}
    ]
    try:
        response = client.chat.completions.create(
            model="Ethel_o3_mini",
            messages=messages
        )
        # Wrap feedback in a <pre> tag to preserve formatting.
        return f"<pre style='white-space: pre-wrap;'>{response.choices[0].message.content.strip()}</pre>"
    except Exception as e:
        logging.error(f"Error during feedback API call: {e}")
        return "Error: Unable to generate feedback."

#
# 4) Instructor authentication via Basic Auth.
#
def is_instructor_authenticated(headers):
    """
    Checks the instructor's credentials by decoding the Basic Auth header and comparing
    the entered values with those stored in the password file.
    """
    logging.info("Received request headers: %s", headers)
    isauth_orig, creds_orig = auth(headers)
    logging.info("Auth returned: isauth=%s, creds=%s", isauth_orig, creds_orig)
    
    auth_header = headers.get("Authorization")
    if not auth_header or not auth_header.startswith("Basic "):
        logging.info("No valid Authorization header provided.")
        return False
    base64_creds = auth_header[len("Basic "):]
    try:
        decoded = base64.b64decode(base64_creds).decode("utf-8")
    except Exception as e:
        logging.info("Error decoding Authorization header: %s", e)
        return False

    parts = decoded.split(":", 1)
    if len(parts) != 2:
        logging.info("Decoded credentials do not contain a colon: %s", decoded)
        return False
    entered_username = parts[0].strip()
    entered_password = parts[1].strip()
    
    expected_username, expected_password = get_instructor_credentials()
    if expected_username is None or expected_password is None:
        logging.info("Expected credentials not available.")
        return False

    logging.info("Login attempt: Entered (%s, %s) | Expected (%s, %s)",
                 entered_username, entered_password, expected_username, expected_password)
    return entered_username == expected_username and entered_password == expected_password

#
# 5) Main HTTP handler.
#
class FeedbackServer(BaseHTTPRequestHandler):
    """
    HTTP server to handle assignment management, student submissions, and feedback display.
    Uses ThreadingHTTPServer for concurrent request handling.
    """
    def set_headers(self, status=200, content_type='text/html'):
        self.send_response(status)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Content-Type', content_type)
        self.end_headers()

    def write_html(self, title, body_html, status=200):
        page = render_page(title, body_html)
        self.set_headers(status=status)
        self.wfile.write(page.encode('utf-8'))

    def send_unauthorized(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm="Manage Assignments"')
        self.send_header('Content-Type', 'text/html')
        self.end_headers()
        self.wfile.write(b"Unauthorized")

    def do_GET(self):
        parsed_url = urlparse(self.path)
        path = parsed_url.path

        # Instructor management interface.
        if path.startswith('/manage'):
            if not is_instructor_authenticated(self.headers):
                self.send_unauthorized()
                return

            assignments = load_assignments()
            # Show assignment details for instructors, including grading instructions and sheet transcription.
            if path == '/manage':
                body = "<h1>Manage Assignments</h1><p><a href='..'>Back to student form</a></p>"
                body += '<a class="button" href="/manage/new">New Assignment</a><br/><br/>'
                if assignments:
                    body += "<ul>"
                    for aid, assignment in assignments.items():
                        status_text = "Active" if assignment.get("active", False) else "Inactive"
                        body += f"<li><strong>{assignment.get('title','Untitled')}</strong> - {status_text}<br>"
                        body += f"<strong>Grading Instructions:</strong> {assignment.get('additional_criteria','')}<br>"
                        body += f"<strong>Sheet Transcription:</strong> {assignment.get('sheet_transcription','')}<br>"
                        body += f"""
                        <form style="display:inline;" action="/manage/toggle" method="post">
                          <input type="hidden" name="id" value="{aid}" />
                          <button class="button" type="submit">Toggle Active</button>
                        </form>
                        <form style="display:inline;" action="/manage/delete" method="post" onsubmit="return confirm('Delete assignment?');">
                          <input type="hidden" name="id" value="{aid}" />
                          <button class="button" type="submit">Delete</button>
                        </form>
                        </li><hr>
                        """
                    body += "</ul>"
                else:
                    body += "<p>No assignments found.</p>"
                self.write_html("Manage Assignments", body)
                return

            # New Assignment Form.
            if path == '/manage/new':
                body = """
                  <h1>New Assignment</h1>
                  <p>
                  Please upload your assignment sheet (like the students would have received).
                  </p><p>
                  In the field "Additional Criteria," you can add instructions for the bot that are
                  not visible to the students.</p>
                  <p><a href="..">Back to student form</a></p>
                  <form action="/manage/new" method="post" enctype="multipart/form-data"
                        onsubmit="showProcessing('newAssignBtn','processingMsg')">
                    <label>Title:</label><br/>
                    <input type="text" name="title" required/><br/><br/>
                    <label>Assignment Sheet (file):</label><br/>
                    <input type="file" name="sheet_file" accept=".pdf,.png,.jpg,.jpeg,.txt,.tex,.ipynb" required/><br/><br/>
                    <label>Additional Criteria (Grading Instructions):</label><br/>
                    <textarea name="criteria" rows="16" cols="80" placeholder="Enter grading instructions here..."></textarea><br/><br/>
                    <label>Active:</label>
                    <input type="checkbox" name="active" checked/><br/><br/>
                    <button id="newAssignBtn" class="button" type="submit">Create Assignment</button>
                  </form>
                  <div id="processingMsg" style="display: none; margin-top: 1em;">
                    <strong>Processing (please be patient) ...</strong>
                  </div>
                """
                self.write_html("New Assignment", body)
                return

            self.write_html("Not Found", "<h1>Not Found</h1>", status=404)
            return

        # Student main page: list active assignments (without grading instructions).
        if path == '/':
            assignments = load_assignments()
            active_assignments = [a for a in assignments.values() if a.get("active", False)]
            body = """<h1>Ethel Assignments</h1>
            <p>You can upload your work here for feedback. The system accepts typeset or handwritten
            solutions in the listed formats. For handwritten solutions, for best results, please write
            in black on plain paper (no lines, frames, etc.).</p><p>
            Please label your solutions corresponding to the exercise sheet, e.g., "Exercise 3a)"</p> 
            Processing can take a little bit of time, please do not reload this page.</p>
            """
            if active_assignments:
                for assignment in active_assignments:
                    body += f"<h2>{assignment.get('title','Untitled')}</h2>"
                    # Do not display additional criteria in the student view.
                    body += f"""
                      <form action="/submit" method="post" enctype="multipart/form-data"
                            onsubmit="showProcessing('submitBtn_{assignment['id']}','processingMsg_{assignment['id']}')">
                        <input type="hidden" name="id" value="{assignment['id']}" />
                        <label>Upload your solution (PDF, PNG, JPG, TXT, TEX, IPYNB):</label><br/>
                        <input type="file" name="solution_file" required/><br/><br/>
                        <button id="submitBtn_{assignment['id']}" class="button" type="submit">Submit</button>
                      </form>
                      <div id="processingMsg_{assignment['id']}" style="display: none; margin-top: 1em;">
                        <strong>Processing (please be patient) ...</strong>
                      </div>
                    """
            else:
                body += "<p>No active assignments at the moment.</p>"
            body += "<hr /><p><a class='button' href='/manage'>Manage Assignments (instructor only)</a></p>"
            self.write_html("Assignments", body)
            return

        self.write_html("Not Found", "<h1>Not Found</h1>", status=404)

    def do_POST(self):
        parsed_url = urlparse(self.path)
        path = parsed_url.path

        # Parse form data (supports both multipart and URL-encoded).
        ctype, pdict = cgi.parse_header(self.headers.get('Content-Type'))
        if ctype == 'multipart/form-data':
            pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
            pdict['CONTENT-LENGTH'] = int(self.headers.get('Content-Length'))
            form = cgi.FieldStorage(fp=self.rfile, headers=self.headers,
                                    environ={'REQUEST_METHOD': 'POST'}, keep_blank_values=True)
        else:
            length = int(self.headers.get('Content-Length', 0))
            post_data = self.rfile.read(length).decode('utf-8')
            form = parse_qs(post_data)

        # Instructor routes.
        if path.startswith('/manage'):
            if not is_instructor_authenticated(self.headers):
                self.send_unauthorized()
                return

            if path == '/manage/new':
                # Process new assignment creation.
                title = get_form_value(form, "title")
                criteria = get_form_value(form, "criteria") or ""
                active = True if get_form_value(form, "active") == "on" else False

                file_item = form['sheet_file'] if "sheet_file" in form else None
                if file_item is None or file_item.filename is None:
                    self.write_html("Error", "<h1>Assignment sheet file is required.</h1>", status=400)
                    return

                # Generate a unique assignment ID.
                assignment_id = str(uuid.uuid4())
                try:
                    # Read the assignment file entirely into memory.
                    file_data = file_item.file.read()
                except Exception as e:
                    logging.error(f"Error reading assignment sheet file: {e}")
                    self.write_html("Error", "<h1>Error reading file.</h1>", status=500)
                    return

                try:
                    # Transcribe the assignment sheet from memory.
                    sheet_transcription = transcribe.transcribe_file(file_data, file_item.filename)
                except Exception as e:
                    logging.error(f"Error transcribing sheet file: {e}")
                    sheet_transcription = "Transcription unavailable."

                # Save assignment metadata (do not store the original file).
                assignment = {
                    "id": assignment_id,
                    "title": title,
                    "sheet_transcription": sheet_transcription,
                    "additional_criteria": criteria,
                    "active": active
                }
                save_assignment(assignment)
                self.send_response(302)
                self.send_header('Location', '/manage')
                self.end_headers()
                return

            if path == '/manage/toggle':
                aid = get_form_value(form, "id")
                assignments = load_assignments()
                if aid not in assignments:
                    self.write_html("Error", "<h1>Assignment not found.</h1>", status=404)
                    return
                assignments[aid]["active"] = not assignments[aid].get("active", False)
                save_assignment(assignments[aid])
                self.send_response(302)
                self.send_header('Location', '/manage')
                self.end_headers()
                return

            if path == '/manage/delete':
                aid = get_form_value(form, "id")
                assignments = load_assignments()
                if aid not in assignments:
                    self.write_html("Error", "<h1>Assignment not found.</h1>", status=404)
                    return
                delete_assignment(aid)
                self.send_response(302)
                self.send_header('Location', '/manage')
                self.end_headers()
                return

            self.write_html("Error", "<h1>Unknown POST request for instructor.</h1>", status=404)
            return

        # Student submission route.
        if path == '/submit':
            aid = get_form_value(form, "id")
            assignments = load_assignments()
            if aid not in assignments:
                self.write_html("Error", "<h1>Assignment not found.</h1>", status=404)
                return
            assignment = assignments[aid]
            if not assignment.get("active", False):
                self.write_html("Error", "<h1>This assignment is not active.</h1>", status=400)
                return

            file_item = form['solution_file'] if "solution_file" in form else None
            if file_item is None or file_item.filename is None:
                self.write_html("Error", "<h1>No file uploaded.</h1>", status=400)
                return

            filename = file_item.filename
            file_ext = os.path.splitext(filename)[1].lower()
            try:
                file_data = file_item.file.read()
            except Exception as e:
                logging.error(f"Error reading uploaded file: {e}")
                self.write_html("Error", "<h1>Error processing file.</h1>", status=500)
                return

            # Transcribe the student's solution.
            if file_ext in [".pdf", ".png", ".jpg", ".jpeg"]:
                try:
                    solution_content = transcribe.transcribe_file(file_data, filename)
                except Exception as e:
                    logging.error(f"Error transcribing solution file: {e}")
                    solution_content = "Transcription unavailable."
            elif file_ext in [".txt", ".tex", ".ipynb"]:
                try:
                    solution_content = file_data.decode('utf-8')
                except Exception as e:
                    logging.error(f"Error decoding text file: {e}")
                    solution_content = "Error decoding file."
            else:
                solution_content = "Unsupported file type."

            course_prompt = get_course_prompt()
            # Generate feedback (the feedback is wrapped in <pre> tags to preserve formatting).
            feedback = generate_feedback(course_prompt,
                                         assignment.get("sheet_transcription", ""),
                                         assignment.get("additional_criteria", ""),
                                         solution_content)
            body = f"""
              <h1>Feedback for {assignment.get('title')}</h1>
              {feedback}
              <br/><br/>
              <a class="button" href="/">Back to Assignments</a>
            """
            self.write_html("Feedback", body)
            return

        self.write_html("Error", "<h1>Unknown POST request.</h1>", status=404)

def run(server_class=ThreadingHTTPServer, handler_class=FeedbackServer, port=8000):
    """
    Initializes and runs the HTTPS server using a threaded HTTP server.
    This allows the server to handle multiple requests concurrently.
    """
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    ssl_context = make_ssl()
    httpd.socket = ssl_context.wrap_socket(httpd.socket, server_side=True)
    container_id = socket.gethostname()
    logging.basicConfig(filename=f"../logs/feedback_{container_id}.log", level=logging.INFO)
    logging.info("=============== Starting feedback server")
    logging.info(f"Starting https on port {port}...")
    try:
        httpd.serve_forever()
    except Exception as e:
        logging.error(f"Unexpected error: {e}", exc_info=True)
    finally:
        httpd.server_close()
        logging.info("Server shutdown completed.")

if __name__ == '__main__':
    run()

