#!/usr/bin/env python3
"""
Module for transcribing binary assignment and solution files into LaTeX using an LLM.

Supported file types:
  - PDF (multipage): Converted to a series of PNG images (one per page) and transcribed page‐by‐page.
  - PNG, JPG, JPEG: Treated as single-page images and transcribed.
  - TXT and LaTeX (.tex): Returned as-is.
  
IPYNB files are not yet supported.
"""

import os
import base64
import shutil
from pdf2image import convert_from_bytes
from PIL import Image
from io import BytesIO
from openai import AzureOpenAI  # Ensure this package is installed
import logging

# Configure a module-specific logger.
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
if not logger.handlers:
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

def transcribe_image(image, page_number, original_filename):
    """
    Transcribe a single PIL image into LaTeX using an Azure OpenAI call.

    The image is converted to PNG format, Base64-encoded, and sent to the LLM
    along with a detailed prompt instructing it to transcribe text, equations,
    and describe figures.

    Parameters:
      image (PIL.Image.Image): The image object.
      page_number (int): The page number (1-indexed).
      original_filename (str): The original filename (for reference).

    Returns:
      str: LaTeX transcription for the image or an error message.
    """
    # Read Azure configuration from file.
    server = None
    api_key = None
    config_file = "../configs/azure_ai_config_o3.txt"
    try:
        with open(config_file, 'r') as file:
            for line in file:
                line = line.strip()
                if line.startswith("server"):
                    server = line.split("=", 1)[1].strip()
                elif line.startswith("key"):
                    api_key = line.split("=", 1)[1].strip()
    except Exception as e:
        logger.error(f"Error reading Azure config: {e}")
        return "Error: Unable to read Azure config."

    if not server or not api_key:
        logger.error("Missing Azure config values (server or key).")
        return "Error: Missing Azure config values (server or key)."

    try:
        client = AzureOpenAI(
            api_version="2024-12-01-preview",
            azure_endpoint=server,
            api_key=api_key
        )
    except Exception as e:
        logger.error(f"Error initializing AzureOpenAI client: {e}")
        return "Error: Unable to initialize AzureOpenAI client."

    # Convert the PIL image to PNG bytes.
    buffer = BytesIO()
    try:
        image.save(buffer, format="PNG")
    except Exception as e:
        logger.error(f"Error saving image to buffer: {e}")
        return "Error: Unable to process image."
    buffer.seek(0)
    image_bytes = buffer.getvalue()
    image_base64 = base64.b64encode(image_bytes).decode("utf-8")

    # Build the transcription prompt.
    prompt = (
        "Your task is to analyze a provided image of an assignment page that may contain handwritten or typeset text, "
        "mathematical equations, and possibly graphs, figures, or diagrams. Please transcribe the content into LaTeX format "
        "as accurately as possible. In your transcription, please:\n\n"
        "1. Convert all text and mathematical expressions (handwritten or typeset) into proper LaTeX code.\n"
        "2. For any graphs, figures, or diagrams, include a detailed description covering:\n"
        "   - The overall structure of the figure.\n"
        "   - Axis labels, units, and numerical values, if applicable.\n"
        "   - Any trends, data points, or relationships shown in the graph.\n"
        "3. Retain the original language of the content without translation.\n"
        "4. Ensure the final output is well-formatted LaTeX that can be compiled, including sections, equations, and figure captions.\n\n"
        "Please provide only the LaTeX output without any additional commentary.\n\n"
    )
    # Create the image object in the expected multimodal format.
    image_data_object = {
        "type": "image_url",
        "image_url": {
            "url": f"data:image/png;base64,{image_base64}"
        }
    }
    messages = [
        {"role": "system", "content": "You faithfully transcribe images into LaTeX."},
        {"role": "user", "content": [
            {"type": "text", "text": prompt},
            image_data_object
        ]}
    ]

    try:
        response_gpt = client.chat.completions.create(
            model="Ethel_4o",
            temperature=0,
            messages=messages
        )
    except Exception as e:
        logger.error(f"Error during API call: {e}")
        return "Error: API call failed."

    try:
        latex_text = response_gpt.choices[0].message.content.strip()
    except Exception as e:
        logger.error(f"Error parsing response: {e}")
        return "Error: Unable to parse API response."

    return latex_text

def transcribe_file(file_data, filename):
    """
    Transcribe a file (binary data) into LaTeX format.

    - For PDFs: Split the PDF into pages, transcribe each page, and concatenate the results.
    - For image files (PNG, JPG, JPEG): Transcribe as a single page.
    - For TXT and LaTeX (.tex) files: Simply decode the file.

    Parameters:
      file_data (bytes): The binary data of the file.
      filename (str): The original filename.

    Returns:
      str: The final LaTeX transcription or an error message.
    """
    ext = os.path.splitext(filename)[1].lower()
    
    if ext == ".pdf":
        try:
            images = convert_from_bytes(file_data)
        except Exception as e:
            return f"Error converting PDF: {e}"
        transcriptions = []
        for i, image in enumerate(images, start=1):
            page_text = transcribe_image(image, i, filename)
            transcriptions.append(page_text)
        return "\n".join(transcriptions)
    
    elif ext in [".png", ".jpg", ".jpeg"]:
        try:
            image = Image.open(BytesIO(file_data))
        except Exception as e:
            return f"Error loading image: {e}"
        return transcribe_image(image, 1, filename)
    
    elif ext in [".txt", ".tex"]:
        try:
            return file_data.decode("utf-8")
        except Exception as e:
            return f"Error decoding text file: {e}"
    
    else:
        return f"Unsupported file type: {ext}"

if __name__ == '__main__':
    print("Transcription module loaded.")

