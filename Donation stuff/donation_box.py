# Project Ethel
# Server for Accepting Media Donations
# Listens to port 8000
#
# Copyright (C) 2023, 2024  Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
from securecalls import make_ssl, auth
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
from urllib.parse import urlparse, parse_qs
import logging
import socket
import os
import json
import re
from cgi import FieldStorage
from io import BytesIO
from datetime import datetime

class MyServer(BaseHTTPRequestHandler):

    def set_headers(self):
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', 'https://ethel.ethz.ch')
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Content-Type', 'application/json')
        self.end_headers() 

    def do_GET(self):
        self.set_headers()

        isauth, user_info = auth(self.headers)
        if not isauth:
            self.wfile.write(json.dumps({'authenticated' : False}).encode())
            return

        userid = user_info['sub']
        upload_dir = f'/donations/{userid}'  # Ensure this directory exists per user

        if self.path.startswith('/delete?'):
            query_components = parse_qs(urlparse(self.path).query)
            if 'file' in query_components:
                filename = query_components['file'][0]
                sanitized_filename = sanitize_filename(filename)
                file_path = os.path.join(upload_dir, sanitized_filename)
                if os.path.exists(file_path):
                    os.remove(file_path)
                    self.wfile.write(json.dumps({'status': 'File deleted successfully'}).encode())
                else:
                    self.wfile.write(json.dumps({'status': 'File not found'}).encode())
                return

        if self.path == '/files':
            file_info_list = []
            if os.path.exists(upload_dir):
                for filename in os.listdir(upload_dir):
                    file_path = os.path.join(upload_dir, filename)
                    size = os.path.getsize(file_path)
                    modification_time = os.path.getmtime(file_path)
                    human_readable_size = human_readable_bytes(size)
                    modification_date = datetime.utcfromtimestamp(modification_time).isoformat() + 'Z'
                    file_info_list.append({'name': filename, 'size': human_readable_size, 'modified': modification_date})
            self.wfile.write(json.dumps(file_info_list).encode())
            return

        self.wfile.write(json.dumps({}).encode())

    def do_POST(self):
        self.set_headers()
        isauth, user_info = auth(self.headers)
        if not isauth:
            response = {'status': 'No files uploaded'}
            self.wfile.write(json.dumps(response).encode())
            return

        userid = user_info['sub']
        upload_dir = f'/donations/{userid}'  # Directory to store uploaded files

        # Handle file uploads
        if self.path == '/upload':
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            save_uploaded_files(post_data, upload_dir, self.headers, user_info)
            response = {'status': 'Files uploaded successfully'}
            self.wfile.write(json.dumps(response).encode())
            return

        response = {'status': 'Unknown request'}
        self.wfile.write(json.dumps(response).encode())


def human_readable_bytes(size):
    # Define file size units
    for unit in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']:
        if size < 1024.0:
            return f"{size:.2f} {unit}" if unit != 'bytes' else f"{int(size)} {unit}"
        size /= 1024.0
    return f"{size:.2f} YB"

def sanitize_filename(filename):
    # Remove leading/trailing whitespace and normalize to a safe length
    filename = filename.strip()[:255]

    # Replace forward slashes with a safe character like underscore
    filename = filename.replace('/', '_')

    # Remove or replace characters that are problematic for shell operations
    filename = re.sub(r'[&|;<>*$!"]', '', filename)

    # Split the filename to preserve the extension
    name_part, dot, extension = filename.rpartition('.')

    # Optional: replace sequences of dots or spaces with a single underscore in the name part
    name_part = re.sub(r'\.+\s*|\s+', '_', name_part)

    # Remove control characters from the name part
    name_part = ''.join(char for char in name_part if ord(char) >= 32 and ord(char) != 127)

    # Avoid hidden files and directories by prefixing with "hidden_" if it starts with a dot
    if name_part.startswith('.'):
        name_part = 'hidden_' + name_part[1:]

    # Reconstruct the filename with the sanitized name part and original extension
    # Only add the dot and extension back if there was an extension originally
    sanitized_filename = name_part + (dot + extension if extension else '')

    return sanitized_filename

def save_uploaded_files(post_data, upload_dir, headers, user_info):

    if not os.path.exists(upload_dir):
        os.makedirs(upload_dir, exist_ok=True)

    user_info_file = upload_dir+".meta"
    if not os.path.exists(user_info_file):
        with open(user_info_file, 'w') as f:
            # Convert the user_info dictionary to a string and write it to the file.
            # You might want to format this in a more readable way, depending on requirements.
            f.write(json.dumps(user_info, indent=4))

    # Emulate a file-like object for FieldStorage
    fs = FieldStorage(fp=BytesIO(post_data), headers=headers, environ={'REQUEST_METHOD': 'POST', 'CONTENT_TYPE': headers['Content-Type']})

    for field in fs.list or []:
        if field.filename:
            file_path = os.path.join(upload_dir, os.path.basename(sanitize_filename(field.filename)))
            with open(file_path, 'wb') as output_file:
                output_file.write(field.file.read())


def run(server_class=ThreadingHTTPServer, handler_class=MyServer, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    
    ssl_context = make_ssl()  
    
    httpd.socket = ssl_context.wrap_socket(httpd.socket, server_side=True)
    logging.info(f'Starting httpsd on port {port}...')
    
    try:
        httpd.serve_forever()
    except Exception as e:
        logging.error(f'An unexpected error occurred: {e}', exc_info=True)
    finally:
        httpd.server_close()
        logging.info('Server shutdown completed.')

#
# Main program
#
container_id = socket.gethostname()
logging.basicConfig(filename=f"../logs/box_{container_id}.log", level=logging.INFO)
logging.info("=============== Starting donation box server")
run()
